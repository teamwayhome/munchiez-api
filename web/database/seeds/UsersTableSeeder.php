<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the users table first
        // User::truncate();

        $faker = \Faker\Factory::create();

        // Let's make sure everyone has the same password and 
        // let's hash it before the loop, or else our seeder 
        // will be too slow.
        $password = Hash::make('wayhome');
        $pin = Hash::make('5426');

        User::create([
            'name' => 'Charles Wahom',
            'phone' => '+254701375980',
            'email' => 'thewahome.cw@gmail.com',
            'password' => $password,
            'type' => 'admin',
            'tenant_id' => '031ba857-4e83-4630-8cc0-592d2009d4be',
            'api_token' => '3S0Iw9Lzh9uPQ8f8tsrjaq7Ma0InBBBNOj1ofB8iaFmts6MrDbso5Yi2G4MC',
            'pin' => $pin,
        ]);

  
    

        // And now let's generate a few dozen users for our app:
        // for ($i = 1; $i < 10; $i++) {
        //     User::create([
        //         'name' => $faker->name,
        //         'email' => $faker->email,
        //         'password' => $password,
        //         'status'=>1
        //     ]);
        // }
    }
}