<?php
use App\UnitOfMeasure;
use Illuminate\Database\Seeder;

class UnitsOfMeasureSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = ['Roll', 'Cups', 'Kenya Shillings', 'Packets', 'Grams', 'Litres', 'Pieces', 'Kilograms'];
        for ($i = 1; $i < sizeof($units); $i++) {
            UnitOfMeasure::create([
                'name' => $units[$i],
                'user_id' => '75c51354-3daa-4e9b-9375-448494a065e1',
                'tenant_id'=> '031ba857-4e83-4630-8cc0-592d2009d4be',
            ]);
        }
    }
}