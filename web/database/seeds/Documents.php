<?php
use App\Document;
use App\Section;
use Illuminate\Database\Seeder;

class Documents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function generateDoc($i){
        if($i % 3 == 0 && $i % 5 ==0){
            return 'quotation';
        }
        else if($i % 3 == 0){
            return 'invoice';
        }
        else if($i % 5 == 0){
            return 'delivery-note';
        }
        else {
            return 'goods-received-note';
        }
    }

    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 15; $i++) {
            Document::create([
                'name' => $faker->name,
                'description' => $faker->sentence,
                'type'=>$this->generateDoc($i),
                'status' => 0,
                'user_id'=>($i>7)?'bafa438d-101f-476b-ab12-350e238c7126':'ac12c36e-d3e1-4ab9-ba48-437e9f5517b7',
                'client_id' => ($i>7)?'28c6ccdc-5b34-4183-9346-62d09144577d':'2789b17f-278d-456b-b078-345e8fd5dfb0',
                'project_id' => ($i>7)?'0d73011c-98b6-4c61-aeff-89ec0b15633e':'05b75cfe-18c9-4bd9-85da-bbdd8c7e12f0'
            ]);
        }
    }

}
