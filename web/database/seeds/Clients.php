<?php
use App\Client;
use Illuminate\Database\Seeder;

class Clients extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Client::truncate();
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 50; $i++) {
            Client::create([
                'name' => $faker->name, 
                'email' => $faker->email, 
                'phone' => $faker->phoneNumber, 
                'description' => $faker->sentence, 
                'website' => $faker->name,
                'user_id'=>($i>25)?'bafa438d-101f-476b-ab12-350e238c7126':'ac12c36e-d3e1-4ab9-ba48-437e9f5517b7',
                'status' => 0 
            ]);
        }
    }
}
