<?php
use App\Project;
use Illuminate\Database\Seeder;

class Projects extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 1; $i < 25; $i++) {
            Project::create([
                'title'=>$faker->word,
                'description'=>$faker->sentence,
                'user_id'=>($i>12)?'bafa438d-101f-476b-ab12-350e238c7126':'ac12c36e-d3e1-4ab9-ba48-437e9f5517b7',
                'client_id' => ($i>12)?'28c6ccdc-5b34-4183-9346-62d09144577d':'2789b17f-278d-456b-b078-345e8fd5dfb0',
                'status'=>0,
            ]);
        }
    }
}
