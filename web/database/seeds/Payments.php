<?php
use App\PAyment;
use Illuminate\Database\Seeder;

class Payments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 25; $i++) {
            Payment::create([
                'user_id'=>($i>12)?'bafa438d-101f-476b-ab12-350e238c7126':'ac12c36e-d3e1-4ab9-ba48-437e9f5517b7',
                'client_id' => ($i>12)?'28c6ccdc-5b34-4183-9346-62d09144577d':'2789b17f-278d-456b-b078-345e8fd5dfb0',
                'project_id' => ($i>12)?'0d73011c-98b6-4c61-aeff-89ec0b15633e':'05b75cfe-18c9-4bd9-85da-bbdd8c7e12f0',
                'narration'=>$faker->sentence,
                'reference'=>$faker->sentence,
                'reference'=>$faker->sentence,
                'method'=>'cash',
                'amount'=>$i*1000,
                'balance'=>$i*100,
                'status'=>0
            ]);
        }
    }
}
