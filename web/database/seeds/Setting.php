<?php
use App\Settings;
use Illuminate\Database\Seeder;

class Setting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        Settings::create([
            'id'=>'6e06ca1b-10ce-4443-aef3-8948ea4c16b6',
            'name' => 'Yummy MunchieZ',
            'initials' => 'YM',
            'tagline' => 'Yummy in Your Tummy',
            'phone' => '+254722774109',
            'email' => 'charlesmwangi104@gmail.com',
            'website' => 'http://thewahome.com',
            'address' => 'P. O. Box 1467 - 00902, Kikuyu, Kenya\nKidfarmaco, Kikuyu',
            'primary_color' => '#800800',
            'secondary_color' => '#FF0021',
            'logo' => '',
            'tax' => '0',
            'document_slogan' => 'Thank you for your business',
            'urls' => '["admin":"https://munchiez-admin.netlify.com","pos":"https://munchiez-pos.netlify.com"]',
        ]);
    }
}