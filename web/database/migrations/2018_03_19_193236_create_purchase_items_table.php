<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->decimal('price', 8, 2);
            $table->integer('quantity')->default('0');

            $table->uuid('unit_of_measure_id');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measure');
            
            $table->uuid('stock_items_id');
            $table->foreign('stock_items_id')->references('id')->on('stock_items');

            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');

            $table->integer('status')->default('0');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}