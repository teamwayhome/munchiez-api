<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *      name: 'quotation',

     * @return void
     */
    public function up()
    {
        Schema::create('process_sections', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('position');
            $table->string('title');
            $table->string('type');

            $table->uuid('process_id');
            $table->foreign('process_id')->references('id')->on('processes');
            
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');

            $table->integer('status')->default('0');
            $table->timestamps();            
            $table->primary('id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_sections');
    }
}