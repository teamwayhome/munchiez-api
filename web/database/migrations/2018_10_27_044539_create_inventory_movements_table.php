<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_movements', function (Blueprint $table) {
            $table->uuid('id');
            $table->integer('quantity')->default('0');
            $table->string('type')->default('Preparation');
            $table->string('direction')->default('Addition');

           
            $table->uuid('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->uuid('units_of_measure_id');
            $table->foreign('units_of_measure_id')->references('id')->on('units_of_measure');
  
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');

            $table->integer('status')->default('0');
            $table->timestamps();            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_movements');
    }
}