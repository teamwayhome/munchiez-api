<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('tagline');
            $table->string('initials');
            $table->string('phone');
            $table->string('email');
            $table->string('website');
            $table->text('address');
            $table->string('primary_color');
            $table->string('secondary_color');
            $table->text('logo');
            $table->string('tax');
            $table->string('document_slogan');
            $table->string('beneficiary')->nullable();
            $table->string('bank_name')->nullable();
            $table->text('bank_address')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_swift')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('bank_branch')->nullable();
            $table->text('urls')->nullable();
            $table->integer('status')->default('0');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}