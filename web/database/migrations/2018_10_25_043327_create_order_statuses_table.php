<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->uuid('id');
            
            $table->string('title')->default('New');

            $table->uuid('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');
    
            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');
            
            $table->integer('status')->default('0');
            $table->timestamps();            
            $table->primary('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
    }
}