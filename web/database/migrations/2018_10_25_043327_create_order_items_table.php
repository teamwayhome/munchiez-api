<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->uuid('id');
            
            $table->decimal('price', 8, 2);
            $table->integer('quantity')->default('0');
            $table->decimal('amount', 8, 2);
            $table->integer('selling_price')->default('0');
            $table->decimal('total', 8, 2);
            
            $table->uuid('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            
            $table->uuid('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            
            $table->uuid('unit_of_measure_id');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measure');
            
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');
            
            $table->integer('status')->default('0');
            $table->timestamps();            
            $table->primary('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}