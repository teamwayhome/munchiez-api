<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('reference')->default('Purchase');
            $table->integer('isTracked')->default('0');
            $table->integer('reorder')->default('0');
            $table->decimal('value', 8, 2);

            $table->uuid('unit_of_measure_id');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measure');

            $table->uuid('process_id');
            $table->foreign('process_id')->references('id')->on('processes');

            $table->uuid('category_id');
            $table->foreign('category_id')->references('id')->on('categories');

            $table->uuid('source_id')->nullable();

            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');

            $table->integer('status')->default('0');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_items');
    }
}