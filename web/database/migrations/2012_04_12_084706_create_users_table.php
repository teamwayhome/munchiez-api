<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //
            $table->uuid('id');
            $table->string('name');
            $table->string('email', 60)->unique();
            $table->string('phone');
            $table->string('password');
            $table->string('pin');
            $table->string('type');
            $table->string('api_token', 60)->unique()->nullable();
            
            $table->uuid('tenant_id');
            $table->foreign('tenant_id')->references('id')->on('settings');
            
            $table->integer('status')->default('0');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

    }
}