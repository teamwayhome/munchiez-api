<?php

namespace App;
use Webpatser\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class StockTake extends Model
{
    public $incrementing = false;

    protected $fillable = [ 'id',
    'comments','cash','till',
    'status','user_id', 'tenant_id'  ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function stockItems()
    {
      return $this->hasMany('App\StockItems');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}