<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class InventoryMovement extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id','quantity', 
        'type', 'direction', 
        'product_id', 
        'units_of_measure_id','status', 'user_id', 'tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });    
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }


}