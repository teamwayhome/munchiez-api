<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class StockItemMovement extends Model
{
  public $incrementing = false;

  public function stockItem()
  {
    return $this->belongsTo('App\StockItem');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public static function boot()
  {
      parent::boot();
      self::creating(function ($model) {
          $model->id = (string) Uuid::generate(4);
      });
  }

  protected $fillable = [
    'id', 
    'quantity',
    'type',
    'direction',
    'units_of_measure_id',
    'stock_items_id',
    'status',
    'user_id',
    'tenant_id',
    'refernce'
  ];


}