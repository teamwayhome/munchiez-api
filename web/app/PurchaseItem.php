<?php

namespace App;
use Webpatser\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    public $incrementing = false;

    protected $fillable = [ 'id',
    'name','price','quantity','unit_of_measure_id',
    'stock_items_id','purchase_id',
    'status','user_id', 'tenant_id'  ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function purchase()
    {
      return $this->belongsTo('App\Purchase');
    }
}