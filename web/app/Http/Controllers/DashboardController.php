<?php

namespace App\Http\Controllers;

use App\Payment;
use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function ratios()
    {
        $all = Shop::count();
        $response = [
            'success' => true,
            'data' => $ratio
        ];
        return response()->json($response, 200);
    }

    public function payments($year)
    {
        $balances = array();
        $amounts = array();
        $totalAmount = 0;
        $totalBalance = 0;

        for ($i = 1; $i < 13; $i++) {
            $x = array(
                'balance' => Payment::where('year', $year)->where('balance', '>', 0)->where('month', $i)->sum('balance'),
                'amount' => Payment::where('year', $year)->where('month', $i)->sum('amount'),
            );
            array_push($balances, $x['balance']);
            array_push($amounts, $x['amount']);
            $totalAmount = array_sum($amounts);
            $totalBalance = array_sum($balances);
        }

        $payments = array(
            'balances' => $balances,
            'amounts' => $amounts
        );

        $totals = array(
            'balances' => $totalBalance,
            'amounts' => $totalAmount
        );

        $results = array(
            'payments' => $payments,
            'totals' => $totals
        );

        $response = [
            'success' => true,
            'data' => $results
        ];
        return response()->json($response, 200);
    }

    public function expected($year)
    {
        $s = 'Jan-' . $year;
        $e = 'Dec-' . $year;
        $startDate = date_create($s);
        $endDate = date_create($e);
        $startDate = date_format($startDate, "Y-m-d");
        $endDate = date_format($endDate, "Y-m-d");
        $expected = Tenant::whereBetween('created_at', [$startDate, $endDate])->sum('rent');
        $response = [
            'success' => true,
            'data' => $expected * 12,
        ];
        return response()->json($response, 200);
    }

}
