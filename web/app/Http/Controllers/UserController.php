<?php

namespace App\Http\Controllers;
use App\Mail\MailModel;
Use App\User;
Use App\Settings;
use Hash;
Use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $response = [
            'success' => true,
            'data' => $users
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $user = User::find($id);
        $response = [
            'success' => true,
            'data' => $user
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $users = User::where('status',0)->where('tenant_id',Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        return response()->json($users, 200);
    }
    
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $data['tenant_id'] = (Auth::user()->tenant_id) ? Auth::user()->tenant_id : $data['tenant_id'];
            $password = str_random(8);
            $digits = 4;
            $pin = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $data['password'] = Hash::make($password);
            $data['pin'] = $pin;
            $users = User::where('status',0)->where('tenant_id',Auth::user()->tenant_id)->where('pin', $pin)->get();
            if (sizeof($users) > 0) {
                $data['pin'] = $pin + 1;
            }
            $user = User::create($data);

            try {
                $response = [
                    'success' => true,
                    'data' => $user
                ];
                return response()->json($response, 200);
            }

            catch(Exception $e) {

                $response = [
                    'success' => false,
                    'message' => 'Your password is: ' . $password,
                    'data' => $user
                ];
                return response()->json($response, 400);
            }
            return response()->json($response, 200);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => false,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $user = User::find($request->id);
            $user->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id =$request->id;
        $user = User::find($id);
        $user->status = 1;
        $user->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}