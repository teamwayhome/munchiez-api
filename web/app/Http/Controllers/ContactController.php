<?php

namespace App\Http\Controllers;

use App\Contact;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $contacts
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $contact = Contact::find($id);
        $contact->client;
        $response = [
            'success' => true,
            'data' => $contact
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $contacts = Contact::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($contacts); $i++) {
            $contacts[$i]->client;
        }
        return response()->json($contacts, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'client_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $contact = Contact::create($data);
            $response = [
                'success' => true,
                'data' => $contact
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'client_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $contact = Contact::find($request->id);
            $contact->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $contact = Contact::find($id);
        $contact->status = 1;
        $contact->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
