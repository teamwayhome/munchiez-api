<?php

namespace App\Http\Controllers;

use App\Client;
use App\Project;
use App\Employee;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Settings;
use Illuminate\Support\Facades\View;
use Mail;
use App\Mail\MailModel;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $clients
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $client = Client::find($id);
        $client->documents;
        $client->payments;
        $client->progress;
        $client->contacts;
        for ($i = 0; $i < sizeof($client->documents); $i++) {
            $client->documents[$i]->user;
            $client->documents[$i]->project;
        }
        for ($i = 0; $i < sizeof($client->payments); $i++) {
            $client->payments[$i]->user;
            $client->payments[$i]->project;
        }
        for ($i = 0; $i < sizeof($client->progress); $i++) {
            $client->progress[$i]->user;
            $client->progress[$i]->project;
        }
        $response = [
            'success' => true,
            'data' => $client
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $clients = Client::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($clients); $i++) {
            $clients[$i]->user;
        }
        return response()->json($clients, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $count = Client::count() + 1;
            $count = str_pad($count, 5, "0", STR_PAD_LEFT);
            $settings = Settings::where('id', $data['tenant_id'])->first();
            $initials = '';
            if ($settings->initials) {
                $initials = $settings->initials . '-';
            }
            $data['reference'] = $initials . $count;
            $client = Client::create($data);
            $response = [
                'success' => true,
                'data' => $client
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $client = Client::find($request->id);
            $client->fill($data)->save();
            $response = [
                'success' => true,
                'data' => $client
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $client = Client::find($id);
        $client->status = 1;
        $client->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function statusReport($id)
    {
        $client = Client::find($id);
        $projects = Project::where([['client_id', '=', $id], ['file_status', '<>', 'Closed']])->get();
        $settings = Settings::where('id', Auth::user()->tenant_id)->first();
        $user = Auth::user();

        $employees = Employee::all();
        $emails = $employees->reject(function ($employee) {
            return $employee->role == 'Executive' || $employee->role == 'General' || $employee->role == 'Accounts';
        })
            ->map(function ($employee, $key) {
                return $employee->email;
            });

        $recipients = $client->email;
        foreach ($emails as $value) {
            $recipients = $recipients . ' , ' . $value;
        }

        $view = View::make('status_report', [
            'projects' => $projects,
            'settings' => $settings,
            'recipients' => $recipients
        ]);


        $contents = (string)$view;

        $toSend = [
            'subject' => 'Pasha CRM Status Report',
            'message' => $contents,
            'senderName' => $user->name,
            'senderEmail' => $user->email
        ];

        Mail::to($client->email)
            ->cc($emails)
            ->send(new MailModel($toSend));

        $response = [
            'success' => true,
            'message' => 'Status Report Sent'
        ];
        return response()->json($response, 200);
    }
}
