<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\MailModel;
use App\Settings;
use App\User;
use Auth;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;
use Log;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            if ($user->api_token == null) {
                $user->generateToken();
            }

            return response()->json([
                'data' => $user->toArray(),
            ]);
        }

        return response()->json(['errors' => 'These credentials do not match our records.'], 422);
    }
    
    public function verify($pin)
    {
        $users = User::where('pin', $pin)->get();
        $size = sizeof($users);
        Log::error(json_encode($users));
        switch ($size) {
            case 1:
                $user = $users[0];
                $user->generateToken();
                return response()->json([
                    'success' => true,
                    'data' => $user->toArray(),
                ]);

            case 0:
                return response()->json(['errors' => 'These credentials do not match our records.'], 422);
            default:
                return response()->json(['errors' => 'These credentials do not match our records.'], 422);

        }
    }

    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();
        if ($user) {
            $user->api_token = null;
            $user->save();
            return response()->json(['data' => 'User logged out.'], 200);
        }else{
            return response()->json(['message' =>  'User not found'], 201);
        }
        return response()->json(['error' => 'User logged out failed'], 200);
    }

        public function change(Request $request)
    {
        $rules = [
            'newPassword' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $user = User::find($request->id);
            $user->password = Hash::make($data['newPassword']);
            $user->save();

            try {
                $settings = Settings::where('id', $user->tenant_id)->first();
                $view = View::make('notifications', 
                    [ 
                        'name' => $user->name, 
                        'password' => $data['newPassword'], 
                        'logo' => $settings->logo,
                        'email' => $user->email, #
                        'recipients'=> $user->email 
                    ]
                );
                $contents = (string)$view;
                $toSend = [
                    'subject' => 'Password Changed',
                    'message' => $contents,
                    'senderName' => $settings->name,
                    'senderEmail' => $settings->email
                ];
                Mail::to($user->email)->send(new MailModel($toSend));
                $response = [
                    'success' => true,
                ];
                return response()->json($response, 200);
            }

            catch(Exception $e) {

                $response = [
                    'success' => false,
                    'message' => 'Your password is: ' . $newPassword,

                ];
                return response()->json($response, 200);
            }
        }
    }    

    public function reset(Request $request)
    {
        $rules = [
            'email' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return response()->json(['errors' => 'These credentials do not match our records.'], 422);
            }
            $newPassword = str_random(8);
            $user->password = Hash::make($newPassword);
            $user->save();

            try {
                $settings = Settings::where('id', $user->tenant_id)->first();
                $view = View::make('notifications', 
                    [ 
                        'name' => $user->name, 
                        'password' => $newPassword, 
                        'logo' => $settings->logo,
                        'email' => $user->email, #
                        'recipients'=> $user->email 
                    ]
                );
                $contents = (string)$view;
                $toSend = [
                    'subject' => 'Password Changed',
                    'message' => $contents,
                    'senderName' => $settings->name,
                    'senderEmail' => $settings->email
                ];
                Mail::to($user->email)->send(new MailModel($toSend));
                $response = [
                    'success' => true,
                ];
                return response()->json($response, 200);
            }

            catch(Exception $e) {

                $response = [
                    'success' => false,
                    'message' => 'Your password is: ' . $newPassword,

                ];
                return response()->json($response, 200);
            }
        }    
    }
}