<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $data['type'] = 'admin';
            $data['tenant_id'] = $data['tenant_id'];
            $data['password'] = Hash::make('123456789');
            $user = User::create($data);
            $response = [
                'success' => true,
                'data' => $user
            ];
            return response()->json($response, 200);
        }
    }

}
