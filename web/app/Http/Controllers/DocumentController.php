<?php

namespace App\Http\Controllers;

use App\Document;
use App\Project;
use App\User;
use App\Mail\MailModel;
use App\Settings;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;

class DocumentController extends Controller
{
    use AuthenticatesUsers;
    public function index()
    {
        $documents = Document::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $documents
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $document = Document::find($id);
        $document->sections;
        $document->user;
        $document->client;
        $document->client->contacts;
        $document->project;

        $response = [
            'success' => true,
            'data' => $document
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $documents = Document::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($documents); $i++) {
            $documents[$i]->user;
            $documents[$i]->client;
            $documents[$i]->project;
        }
        return response()->json($documents, 200);
    }

    public function service($service, $project)
    {
        $documents = Document::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->where('service', $service)->where('project_id', $project)->orderBy('created_at', 'desc')->get();
        for ($i = 0; $i < sizeof($documents); $i++) {
            $documents[$i]->sections;
        }
        return response()->json($documents, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $count = Document::where('type', $data['type'])->count() + 1;
            $documentType = strtoupper(substr($data['type'], 0, 3));
            $count = str_pad($count, 5, "0", STR_PAD_LEFT);
            $settings = Settings::where('id', $data['tenant_id'])->first();
            $initials = '';
            if ($settings->initials) {
                $initials = $settings->initials . '-';
            }

            $data['reference'] = $initials . $documentType . '-' . $count;

            //Set approval status for initial requisition
            if ($data['type'] == 'requisition') {
                $data['document_status'] = 'Awaiting Approval';
            }

            $document = Document::create($data);
            $response = [
                'success' => true,
                'data' => $document
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'type' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $document = Document::find($request->id);
            $document->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $document = Document::find($id);
        $currentUser = Auth::user()->id;
        $documentUser = $document->user_id;
        if ($currentUser !== $documentUser) {
            $response = [
                'success' => false,
                'message' => 'You are not the author of this document'
            ];
            return response()->json($response, 424);
        }
        $document->status = 1;
        $document->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function notify($id)
    {
        $document = Document::find($id);
        $users = User::where('status', 0)->get();
        $columns = array('Description', 'Qty', 'Rate', 'Amount');
        $currentUser = Auth::user();
        $emails = array();
        foreach ($users as $user) {
            if ($user->email !== $currentUser->email) {
                array_push($emails, $user->email);
            }
        }
        $sections = $document->sections;
        $total = 0;
        for ($i = 0; $i < sizeof($sections); $i++) {
            $total = $total + $sections[$i]['amount'];
        }
        $review = url('/') . '/' . 'requisition/review/' . $id;

        $recipients = '';
        foreach ($emails as $value) {
            $recipients = $recipients . ' , ' . $value;
        }

        $view = View::make('requisition_notifications', [
            'document' => $document,
            'review' => $review,
            'columns' => $columns,
            'total' => $total,
            'recipients' => $recipients
        ]);
        $contents = (string)$view;

        $toSend = [
            'subject' => 'New Requisition Pasha CRM Platform',
            'message' => $contents,
            'senderName' => $user->name,
            'senderEmail' => $user->email
        ];

        Mail::to($emails[0])
            ->cc(array_slice($emails, 1))
            ->send(new MailModel($toSend));

        $response = [
            'success' => true,
            'data' => $document
        ];
        return response()->json($response, 200);
    }

    public function changeStatus($id, $status, $user, $reason)
    {
        $users = User::where('status', 0)->get();
        $document = Document::find($id);
        $document->document_status = $status;
        $document->document_user = $user->name;
        if ($reason) {
            $document->comments = $document->comments . ' ... ' . $status . ' by ' . $user->name . ' - ' . $reason;
        }

        $document->save();
        $emails = array();
        foreach ($users as $user) {
            array_push($emails, $user->email);
        }

        $settings = Settings::where('id', Auth::user()->tenant_id)->first();
        $recipients = $document->user->email;
        foreach ($emails as $value) {
            $recipients = $recipients . ' , ' . $value;
        }

        $view = View::make('requisition_approval_notifications', ['document' => $document, 'user' => $user->name, 'recipients' => $recipients]);
        $contents = (string)$view;

        $toSend = [
            'subject' => 'Requisition Approval Action Notification',
            'message' => $contents,
            'senderName' => $user->name,
            'senderEmail' => $user->email
        ];

        Mail::to($document->user->email)
            ->cc($emails)
            ->send(new MailModel($toSend));

    }

    public function status(Request $request)
    {
        $this->changeStatus($request['id'], $request['status'], Auth::user(), null);
        $response = [
            'success' => true,
            'message' => 'Requisition has been ' . $request['status'],
        ];
        return response()->json($response, 200);
    }

    public function review($id)
    {
        $columns = array('Description', 'Qty', 'Rate', 'Amount');
        $document = Document::find($id);

        return View::make('requisition_review', [
            'document' => $document,
            'columns' => $columns,
            'recipients' => null
        ]);
    }

    public function requisition(Request $request)
    {
        $status = 'Approved';
        $reason = null;
        $password = $request->get('password');
        $email = $request->get('email');
        $approve = $request->get('approve');
        $reject = $request->get('reject');
        $reason = $request->get('reason');
        $id = $request->get('id');
        $document = Document::find($id);
        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            if ($reject) {
                $status = 'Rejected';
                $reason = $reason;
            }
            $this->changeStatus($id, $status, $user, $reason);
            return View::make('requisition_approval_notifications', ['document' => $document, 'user' => $user->name, 'recipients' => null]);
        } else {
            echo '<h1>Your email/password did not match our records</h1>';
        }
    }
}
