<?php

namespace App\Http\Controllers;

use App\Process;
use App\ProcessSection;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

class ProcessController extends Controller
{
    public function index()
    {
        $processes = Process::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($processes); $i++) {
            $processes[$i]->sections = sizeof($processes[$i]->sections);
            $processes[$i]->add = json_decode($processes[$i]->add);
            $processes[$i]->deduct = json_decode($processes[$i]->deduct);
        }
        $response = [
            'success' => true,
            'data' => $processes
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $process = Process::find($id);
        $process->sections;
        $process['add'] = json_decode($process->add);
        $process['deduct'] = json_decode($process->deduct);


        $response = [
            'success' => true,
            'data' => $process
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize = 100)
    {
        $processes = Process::where('status', 0)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize);
            
        for ($i = 0; $i < sizeof($processes); $i++) {
            $processes[$i][steps] = sizeof($processes[$i]->sections);
            $processes[$i]->add = json_decode($processes[$i]->add);
            $processes[$i]->deduct = json_decode($processes[$i]->deduct);
        }
        
        $response = [
            'success' => true,
            'data' => $processes
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'multiplier' => 'required',
            'add' => 'required',
            'sections' => 'required',
            'deduct' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = [];
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $data['deduct'] = json_encode($request->deduct);
            $data['add'] = json_encode($request->add);
            $data['multiplier'] = $request->multiplier;
            $data['name'] = $request->name;
            $process = Process::create($data);
            foreach ($request->sections as $section) {
                $section['tenant_id'] = Auth::user()->tenant_id;
                $section['user_id'] = Auth::user()->id;
                $section['process_id'] = $process->id;
                ProcessSection::create($section);
            }
            $response = [
                'success' => true,
                'data' => $process
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'multiplier' => 'required',
            'add' => 'required',
            'sections' => 'required',
            'deduct' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $process = Process::find($request->id);
            $process->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $process = Process::find($id);
        $process->status = 1;
        $process->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }
}