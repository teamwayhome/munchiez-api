<?php

namespace App\Http\Controllers;
use App\Section;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class SectionController extends Controller
{
    public function index()
    {
        $sections = Section::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $sections
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $section = Section::find($id);
        $response = [
            'success' => true,
            'data' => $section
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $sections = Section::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        return response()->json($sections, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'amount' => 'required',
            'position' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $section = Section::create($request->all());
            $response = [
                'success' => true,
                'data' => $section
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'amount' => 'required',
            'position' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $section = Section::find($request->id);
            $section->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $section = Section::find($id);
        $section->status = 1;
        $section->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
