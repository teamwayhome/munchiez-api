<?php

namespace App\Http\Controllers;

use App\Upload;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadController extends Controller
{

    public function clean($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public function index()
    {
        $uploads = Upload::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($uploads); $i++) {
            $uploads[$i]->project;
        }
        $response = [
            'success' => true,
            'data' => $uploads
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $upload = Upload::find($id);
        $upload->project;
        $response = [
            'success' => true,
            'data' => $upload
        ];
        return response()->json($response, 200);
    }

    public function project($id)
    {
        $uploads = Upload::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->where('project_id', $id)->orderBy('created_at', 'desc')->get();
        for ($i = 0; $i < sizeof($uploads); $i++) {
            $uploads[$i]->user;
        }
        return response()->json($uploads, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'project_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $tenant_id = Auth::user()->tenant_id;
            if (!$request->hasFile('data')) {
                return response()->json(['upload_file_not_found'], 400);
            }
            $file = $request->file('data');
            if (!$file->isValid()) {
                return response()->json(['invalid_file_upload'], 400);
            }

            $path = public_path() . '/uploads' . '/' . $tenant_id . '/files' . '/' . $request->project_id . '/';
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move($path, $filename);

            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = $tenant_id;
            $data['name'] = $request->name;
            $data['link'] = url('/') . '/' . 'uploads' . '/' . $tenant_id . '/files' . '/' . $request->project_id . '/' . $filename;
            $data['project_id'] = $request->project_id;

            $upload = Upload::create($data);
            $response = [
                'success' => true,
                'data' => $upload,
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'project_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $upload = Upload::find($request->id);
            $upload->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function fields(Request $request)
    {
        $rules = [
            'id' => 'required',
            'fields' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $upload = Upload::find($request->id);
            $upload->fill($data)->save();
            $response = [
                'success' => true,
                'data' => $upload,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $upload = Upload::find($id);
        $upload->status = 1;
        $upload->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
