<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Tenant;
use App\OrderItem;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i=0; $i < sizeof($payments); $i++) { 
            $payments[$i]->order;
            $payments[$i]->items = OrderItem::where('order_id', $payments[$i]->order_id)->count();
        }
        $response = [
            'success' => true,
            'data' => $payments
        ];
        return response()->json($response, 200);
    }

    public function balance(Request $request)
    {
        $paymentRecord = Payment::where('year', $request->year)
            ->where('month', $request->month)
            ->where('client_id', $request->client_id)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->first();
        $client = Tenant::find($request->client_id);
        $rent = $client->rent;

        if (sizeof($paymentRecord) == 0) {
            $balance = $rent;
        } else {
            $balance = $rent - $paymentRecord->amount;
        }
        return response()->json(['balance' => $balance, 'rent' => $rent], 200);
    }

    public function show($id)
    {
        $payment = Payment::find($id);
        $payment->project;
        $payment->client;
        $response = [
            'success' => true,
            'data' => $payment
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize, $from, $to)
    {
        $startDate = date_create($from);
        $endDate = date_create($to);
        $startDate = date_format($startDate, "Y-m-d");
        $endDate = date_format($endDate, "Y-m-d");
        $payments = Payment::whereBetween('created_at', [$startDate, $endDate])
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($payments); $i++) {
            $payments[$i]->client;
            $payments[$i]->project;
        }
        return response()->json($payments, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'client_id' => 'required',
            'project_id' => 'required',
            'method' => 'required',
            'narration' => 'required',
            'balance' => 'required',
            'amount' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['status'] = 0;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $payment = Payment::create($data);
            $response = [
                'success' => true,
                'data' => $payment
            ];
            return response()->json($response, 200);
        }

        $response = [
            'success' => true,
            'data' => $payment
        ];
        return response()->json($response, 201);
    }
}