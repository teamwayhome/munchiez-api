<?php

namespace App\Http\Controllers;
Use App\Payment;
Use App\Tenant;
Use App\Shop;


use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $searchTerm = strtolower($request->searchTerm);

        $shops = Shop::where('name', 'LIKE', "%$searchTerm%")
        ->orWhere('floor', 'LIKE', "%$searchTerm%")
        ->get();
       
        $tenants = Tenant::where('businessName', 'LIKE', "%$searchTerm%")
        ->orWhere('contactName', 'LIKE', "%$searchTerm%")
        ->orWhere('phoneNumber', 'LIKE', "%$searchTerm%")
        ->orWhere('address', 'LIKE', "%$searchTerm%")
        ->orWhere('rent', 'LIKE', "%$searchTerm%")
        ->get();
       
        $payments = Payment::where('reference', 'LIKE', "%$searchTerm%")
        ->orWhere('amount', 'LIKE', "%$searchTerm%")
        ->orWhere('year', 'LIKE', "%$searchTerm%")
        ->orWhere('month', 'LIKE', "%$searchTerm%")
        ->orWhere('balance', 'LIKE', "%$searchTerm%")
        ->get();

        $shopsCount = sizeof($shops);
        $tenantsCount = sizeof($tenants);
        $paymentsCount = sizeof($payments);
        $resultsCount = $shopsCount + $tenantsCount + $paymentsCount;

        $shopsResults = array(
            'count' => $shopsCount, 
            'items' => $shops, 
        );
        $paymentsResults = array(
            'count' =>$paymentsCount, 
            'items' => $payments, 
        );
        $tenantsResults = array(
            'count' =>$tenantsCount, 
            'items' => $tenants, 
        );

        $results = array(
            'shops' =>$shopsResults, 
            'payments' => $paymentsResults, 
            'tenants' => $tenantsResults, 
        );

        $response = [
            'success' => true,
            'count' => $resultsCount,
            'data' => $results
        ];
        return response()->json($response, 200);
    }
}
