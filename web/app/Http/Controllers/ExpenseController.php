<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Tenant;
use App\OrderItem;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses = Expense::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i=0; $i < sizeof($expenses); $i++) { 
            $expenses[$i]->user;
        }
        $response = [
            'success' => true,
            'data' => $expenses
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $expense = Expense::find($id);
        $expense->user;
        $response = [
            'success' => true,
            'data' => $expense
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize, $from, $to)
    {
        $startDate = date_create($from);
        $endDate = date_create($to);
        $startDate = date_format($startDate, "Y-m-d");
        $endDate = date_format($endDate, "Y-m-d");
        $expenses = Expense::whereBetween('created_at', [$startDate, $endDate])
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($expenses); $i++) {
            $expenses[$i]->client;
            $expenses[$i]->project;
        }
        return response()->json($expenses, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'description' => 'required',
            'amount' => 'required',
            'method' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $expense = Expense::create($data);
            $response = [
                'success' => true,
                'data' => $expense
            ];
            return response()->json($response, 200);
        }

        $response = [
            'success' => true,
            'data' => $expense
        ];
        return response()->json($response, 201);
    }
}