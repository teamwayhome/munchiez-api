<?php

namespace App\Http\Controllers;

use App\StockItem;
use App\UnitOfMeasure;
use App\Process;
use App\StockItemMovement;
use App\ProcessSection;
use App\StockTake;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

class StockItemController extends Controller
{
    public function index()
    {
        $stockItems = StockItem::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($stockItems); $i++) {
            $stockItems[$i]->unit = UnitOfMeasure::find($stockItems[$i]->unit_of_measure_id);
            $process = Process::find($stockItems[$i]->process_id);
            $process['add'] = json_decode($process['add']);
            $process['deduct'] = json_decode($process['deduct']);
            $process['sections'] = ProcessSection::where('status', 0)->where('process_id', $process->id)->get(); 
            $stockItems[$i]->category;
            $stockItems[$i]->process = $process;
        }
        $response = [
            'success' => true,
            'data' => $stockItems
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $stockItem = StockItem::find($id);
        $stockItem->unit = UnitOfMeasure::where('status', 0)->where('id', $stockItem->unit_of_measure_id)->first();
        $stockItem->process = Process::where('status', 0)->where('id', $stockItem->process_id)->first();
        $stockItem->category;
        $stockItem->source = StockItem::find($stockItem->source_id);


        $response = [
            'success' => true,
            'data' => $stockItem
        ];
        return response()->json($response, 200);
    }

    public function history($id)
    {
        $stockItem = StockItem::find($id);
        $movements = StockItemMovement::where('status', 0)->where('stock_items_id', $id)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($movements); $i++) {
            $movements[$i]->user;
            $movements[$i]->unit = UnitOfMeasure::where('status', 0)->where('id', $movements[$i]['units_of_measure_id'])->first();
        }

        $response = [
            'success' => true,
            'data' => [
                'stockItem' => $stockItem,
                'history' => $movements
            ]
        ];

        return response()->json($response, 200);
    }

    public function pages($pageSize = 100)
    {
        $stockItems = StockItem::where('status', 0)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize);
            
        for ($i = 0; $i < sizeof($stockItems); $i++) {
            $stockItems[$i][steps] = sizeof($stockItems[$i]->sections);
            $stockItems[$i]->add = json_decode($stockItems[$i]->add);
            $stockItems[$i]->deduct = json_decode($stockItems[$i]->deduct);
        }
        
        $response = [
            'success' => true,
            'data' => $stockItems
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'category' => 'required',
            'unit' => 'required',
            'reorder' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = [];
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $data['unit_of_measure_id'] = $request->unit;
            $data['name'] = $request->name;
            $data['category_id'] = $request->category;
            $data['reference'] = 'Purchase';
            $data['value'] = 0;
            $data['process_id'] = 'd16851d5-f789-400f-b7e6-1748a99f29d1';
            $data['reorder'] = $request->reorder;


            if ($request->source != '') {
                $data['process_id'] = $request->process;
                $data['source_id'] = $request->source;
                $data['reference'] = 'Preparation';
                $data['isTracked'] = $request->isTracked;
            }
  
            $stockItem = StockItem::create($data);
            
            $response = [
                'success' => true,
                'data' => $stockItem
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'category' => 'required',
            'unit' => 'required',
            'reorder' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = [];
            $data['unit_of_measure_id'] = $request->unit;
            $data['name'] = $request->name;
            $data['category_id'] = $request->category;
            $data['source_id'] = $request->source;
            $data['process_id'] = ($request->process) ? $request->process : 'd16851d5-f789-400f-b7e6-1748a99f29d1' ;
            $data['isTracked'] = $request->isTracked;

            $stockItem = StockItem::find($request->id);
            $stockItem->fill($data)->save();
            
            $response = [
                'success' => true,
                'data' => $stockItem
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $stockItems = StockItem::find($id);
        $stockItems->status = 1;
        $stockItems->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function stocktake()
    {
        $stockItems = StockItem::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $previousStockTake = StockTake::latest('created_at')->first();
        $records = [];
        if ($previousStockTake != '') {
            $records = StockItemMovement::where('refernce', $previousStockTake->id)->get();
        }

        for ($i = 0; $i < sizeof($stockItems); $i++) {
            $stockItems[$i]->unit = UnitOfMeasure::find($stockItems[$i]['unit_of_measure_id']);
        }


        $response = [
            'success' => true,
            'data' => [
                'previous' => $records,
                'stockItems' => $stockItems,
                'record' => $previousStockTake
            ]
        ];
        return response()->json($response, 200);
    }

    public function records()
    {
        $records = StockTake::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($records); $i++) {
            $records[$i]->user; 
        }

        
        $response = [
            'success' => true,
            'data' => $records
        ];
        return response()->json($response, 200);
    }

    public function details($id)
    {
        $stockTake = StockTake::find($id);
        $stockTake->user;

        $records = StockItemMovement::where('status', 0)->where('refernce', $id)->get();
        for ($i = 0; $i < sizeof($records); $i++) {
            $records[$i]->unit = UnitOfMeasure::find($records[$i]['units_of_measure_id']);
            $records[$i]->stockItem = StockItem::find($records[$i]['stock_items_id']);
        }
        
        $response = [
            'success' => true,
            'data' => [
                'detail' => $stockTake,
                'records' => $records
            ]
        ];
        return response()->json($response, 200);
    }

    public function record(Request $request)
    {
        $rules = [
            'cash' => 'required',
            'till' => 'required',
            'records' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $stockTake = StockTake::create($data);

            foreach ($request->records as $record) {
                $item['tenant_id'] = Auth::user()->tenant_id;
                $item['user_id'] = Auth::user()->id;
                $item['refernce'] = $stockTake->id;
                $item['stock_items_id'] = $record['id'];
                $item['quantity'] = $record['quantity'];
                $item['units_of_measure_id'] = $record['unit_of_measure_id'];
                $item['direction'] = 'None';
                $item['type'] = 'Stock Take';
                StockItemMovement::create($item);
            }
            
            $response = [
                'success' => true,
                'data' => $stockTake
            ];
            return response()->json($response, 200);
        }
    }

    public function prepare(Request $request)
    {
        $rules = [
            'stock_item_id' => 'required',
            'add' => 'required',
            'deduct' => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            try {
                $data = $request->all();
                $data['user_id'] = Auth::user()->id;
                $data['tenant_id'] = Auth::user()->tenant_id;
                $stockItem = StockItem::find($request->stock_item_id);
                $sourceStockItem = StockItem::find($stockItem->source_id);

                
                if ($sourceStockItem['value'] < $request->deduct) {
                    
                    $outOfStockItems = [];
                    $set['name'] = $sourceStockItem['name'];
                    $set['unit'] = UnitOfMeasure::find($sourceStockItem['unit_of_measure_id'])->name;
                    $set['value'] = $sourceStockItem['value'];
                    $set['required'] = $request->deduct;
                    array_push($outOfStockItems, $set);

                    return response()->json([
                        'success' => false,
                        'data' => $outOfStockItems,
                        'source'=> $sourceStockItem
                    ], 200);
                }

                $sourceStockItem['value'] = $sourceStockItem['value'] - $request->deduct;
                $sourceStockItem->save();

                $stockItem['value'] = $stockItem['value'] + $request->add;
                $stockItem->save();

                $reference = json_encode($request->reference);

                $deduction['quantity'] = $request->deduct;
                $deduction['refernce'] = $reference;
                $deduction['stock_items_id'] = $sourceStockItem['id'];
                $deduction['units_of_measure_id'] = $sourceStockItem['unit_of_measure_id'];
                $deduction['user_id'] = Auth::user()->id;
                $deduction['tenant_id'] = Auth::user()->tenant_id;
                $deduction['direction'] = 'Deduction';
                StockItemMovement::create($deduction);

                $addition['quantity'] = $request->add;
                $addition['refernce'] = $reference;
                $addition['stock_items_id'] = $stockItem['id'];
                $addition['units_of_measure_id'] = $stockItem['unit_of_measure_id'];
                $addition['user_id'] = Auth::user()->id;
                $addition['tenant_id'] = Auth::user()->tenant_id;
                $addition['direction'] = 'Addition';
                StockItemMovement::create($addition);

                return response()->json([
                    'success' => true,
                ], 200);

        
            } catch (Exception $e) {
                
                return response()->json([
                    'success' => false,
                    'outOfStockItems' => $outOfStockItems,
                    'exception' => $e,
                ], 500);
            }
        }
    }
}