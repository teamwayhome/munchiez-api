<?php

namespace App\Http\Controllers;

use App\Imports\RatesImport;
use App\UnitOfMeasure;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class RateController extends Controller
{
    public function index()
    {
        $groups = Rate::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->select('group')->groupBy('group')->get();
        for ($i = 0; $i < sizeof($groups); $i++) {
            $rates = Rate::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->where('group', $groups[$i]['group'])->orderBy('created_at', 'desc')->get();
            $groups[$i]['rates'] = $rates;
        }
        $response = [
            'success' => true,
            'data' => $groups
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $rate = Rate::find($id);

        $response = [
            'success' => true,
            'data' => $rate
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $rates = Rate::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        return response()->json($rates, 200);
    }

    public function group($group)
    {
        $rates = Rate::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->where('group', $group)->orderBy('created_at', 'desc')->get();
        return response()->json($rates, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'description' => 'required',
            'group' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $rate = Rate::create($data);
            $response = [
                'success' => true,
                'data' => $rate
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'description' => 'required',
            'group' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $rate = Rate::find($request->id);
            $rate->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $rate = Rate::find($id);
        $rate->status = 1;
        $rate->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function upload(Request $request)
    {
        if (!$request->hasFile('data')) {
            return response()->json(['upload_file_not_found'], 400);
        }
        $file = $request->file('data');
        if (!$file->isValid()) {
            return response()->json(['invalid_file_upload'], 400);
        }
        $tenant_id = Auth::user()->tenant_id;

        $path = public_path() . '/storage' . '/' . $tenant_id . '/';
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $file->move($path, $filename);

        try {
            Excel::import(new RatesImport, $filename);
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = [
                'success' => true,
                'message' => $e->getMessage(),
            ];
            return response()->json($response, 422);
        }

    }
}
