<?php

namespace App\Http\Controllers;

use App\Category;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $category
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $category = Category::find($id);
        $response = [
            'success' => true,
            'data' => $category
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $categories = Category::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        return response()->json($categories, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $category = Category::create($data);
            $response = [
                'success' => true,
                'data' => $category
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $category = Category::find($request->id);
            $category->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $category = Category::find($id);
        $category->status = 1;
        $category->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }
}