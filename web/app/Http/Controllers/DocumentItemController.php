<?php

namespace App\Http\Controllers;

use App\DocumentItem;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentItemController extends Controller
{
    public function index()
    {
        $documentItem = DocumentItem::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $documentItem
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $service = DocumentItem::find($id);

        $response = [
            'success' => true,
            'data' => $service
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $documentItem = DocumentItem::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        return response()->json($documentItem, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'headers' => 'required',
            'display' => 'required',
            'description' => 'required',
            'icon' => 'required',
            'calculates' => 'required',
            'columns' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $service = DocumentItem::create($data);
            $response = [
                'success' => true,
                'data' => $service
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'headers' => 'required',
            'display' => 'required',
            'description' => 'required',
            'icon' => 'required',
            'calculates' => 'required',
            'columns' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $service = DocumentItem::find($request->id);
            $service->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $service = DocumentItem::find($id);
        $service->status = 1;
        $service->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }
}
