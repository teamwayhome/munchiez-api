<?php

namespace App\Http\Controllers;

use App\Progress;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProgressController extends Controller
{
    public function index()
    {
        $progressItems = Progress::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        $response = [
            'success' => true,
            'data' => $progressItems
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $progress = Progress::find($id);
        $progress->user;
        $progress->client;
        $progress->project;

        $response = [
            'success' => true,
            'data' => $progress
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $progressItems = Progress::where('status', 0)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($progressItems); $i++) {
            $progressItems[$i]->user;
            $progressItems[$i]->client;
            $progressItems[$i]->project;
        }
        return response()->json($progressItems, 200);
    }

    public function service($service, $project)
    {
        $progressItems = Progress::where('status', 0)
            ->where('service', $service)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->where('project_id', $project)
            ->orderBy('created_at', 'desc')->get();
        for ($i = 0; $i < sizeof($progressItems); $i++) {
            $progressItems[$i]->user;
            $progressItems[$i]->client;
            $progressItems[$i]->project;
        }
        return response()->json($progressItems, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'description' => 'required',
            'stage' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $progress = Progress::create($data);
            $response = [
                'success' => true,
                'data' => $progress
            ];
            return response()->json($response, 201);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $progress = Progress::find($id);
        $progress->status = 1;
        $progress->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
