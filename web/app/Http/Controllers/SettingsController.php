<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class SettingsController extends Controller
{
    public function logo(Request $request)
    {
        $tenant_id = Auth::user()->tenant_id ;
        if (!$request->hasFile('image')) {
            return response()->json([
                'success' => false,
                'message' => 'upload file not found'
            ], 400);
        }

        $file = $request->file('image');
        if (!$file->isValid()) {
             return response()->json([
                'success' => false,
                'message' => 'invalid file upload'
            ], 422);
            return response()->json(['invalid file upload'], 422);
        }

        try {
            $path = public_path('/images' . '/') . $tenant_id . '/logo' . '/';
            $filename = time() . '.' . $file->getClientOriginalExtension();
            
            $file->move($path, $filename);

            $logourl = url('/') . '/images/' . $tenant_id . '/logo/' . $filename;
            $settings = Settings::where('id', $tenant_id)->first();
            $settings->logo = $logourl;
            $settings->save();
            return response()->json([
                'success' => true,
                'message' => 'You have successfully uploaded an image',
                'data' => $settings->logo
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'success' => 'failed to upload the image',
                'data' => $e
            ], 400);
        }

     
    }

    public function index()
    {
        $settings = Settings::where('status', 0)->where('id', Auth::user()->tenant_id)->first();
        $response = [
            'success' => true,
            'data' => $settings
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'tagline' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255|unique:settings',
            'website' => 'required',
            'address' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $string = $data['name'];
            $expr = '/(?<=\s|^)[a-z]/i';
            preg_match_all($expr, $string, $matches);
            $result = implode('', $matches[0]);
            $data['initials'] = strtoupper($result);
            $data['primary_color'] = "#308ee0";
            $data['logo'] = "Not Set";
            $data['document_slogan'] = "Not Set";
            $data['tax'] = 0;
            $data['secondary_color'] = "#cd2007";
            $settings = Settings::create($data);
            $response = [
                'success' => true,
                'data' => $settings
            ];
            return response()->json($response, 200);
        }
    }    

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'tagline' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'website' => 'required',
            'address' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $settings = Settings::find($request->id);
            $settings->fill($data)->save();
            $response = [
                'success' => true,
                'data' => $settings
            ];
            return response()->json($response, 201);
        }
    }
}