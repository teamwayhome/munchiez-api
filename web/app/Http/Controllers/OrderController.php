<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\OrderStatus;
use App\Settings;
use App\Payment;
use App\Product;
use App\InventoryMovement;
use App\UnitOfMeasure;
use App\Category;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($orders); $i++) {
            $orders{$i}->orderItems;
            $orders{$i}->payments;
            $orders{$i}->status = OrderStatus::where('order_id', $orders{$i}->id)->latest('created_at')->first();
            $orders{$i}->user;
        }
        $response = [
            'success' => true,
            'data' => $orders
        ];
        return response()->json($response, 200);
    }    

    public function items()
    {
        $orderItems = OrderItem::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($orderItems); $i++) {
            $orderItems{$i}->order = Order::find($orderItems{$i}->order_id);
            $orderItems{$i}->product = Product::find($orderItems{$i}->product_id);
            $orderItems{$i}->category = Category::find($orderItems{$i}->product->category_id);
            $orderItems{$i}->user;
        }
        $response = [
            'success' => true,
            'data' => $orderItems
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $order = Order::find($id);
        $order->user;
        $order->orderItems;
        $order->payments;
        $order->status = OrderStatus::where('order_id', $order->id)->latest('created_at')->first();
        for ($i = 0; $i < sizeof($order->orderItems); $i++) {
            $order->orderItems{$i}->product = Product::find($order->orderItems{$i}->product_id);
            $order->orderItems{$i}->product['type'] = Category::find($order->orderItems{$i}->product['category_id']);
            $order->orderItems{$i}->product['unit'] = UnitOfMeasure::find($order->orderItems{$i}->product['unit_of_measure_id']);
        }

        $response = [
            'success' => true,
            'data' => $order
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $orders = Order::where('status', 0)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($orders); $i++) {
            $orders[$i]->user;
            $orders[$i]->employee;
        }
        return response()->json($orders, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'type' => 'required',
            'items' => 'required',
            'value' => 'required',
            'payment' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = [];
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['type'] = $request->type;
            $count = Order::where('type', $data['type'])->count() + 1;
            $documentType = strtoupper(substr($data['type'], 0, 3));
            $count = str_pad($count, 5, "0", STR_PAD_LEFT);
            $settings = Settings::where('id', $data['tenant_id'])->first();
            $initials = '';
            if ($settings->initials) {
                $initials = $settings->initials . '-';
            }
            $data['reference'] = $initials . $documentType . '-' . $count;
            $data['value'] = $request->value;
     
            try {
                $order = Order::create($data);

                if ($request->status != '') {
                    $orderStatus['title'] = $request->status;
                } else {
                    $orderStatus['title'] = 'New';
                }
    
                $orderStatus['user_id'] = Auth::user()->id;
                $orderStatus['tenant_id'] = Auth::user()->tenant_id;
                $orderStatus['order_id'] = $order->id;
                OrderStatus::create($orderStatus);
    
                foreach ($request->items as $item) {
                    $item['tenant_id'] = Auth::user()->tenant_id;
                    $item['user_id'] = Auth::user()->id;
                    $item['order_id'] = $order->id;
                    $item['unit_of_measure_id'] = $item['unit_of_measure_id'];
                    $item['selling_price'] = $item['sellingPrice'];
                    $item['product_id'] = $item['id'];
                    $item['total'] = $item['total'];
                    $item['price'] = $item['price'];
                    $item['quantity'] = $item['quantity'];
                    $item['amount'] = $item['price'];
                    OrderItem::create($item);

                    $product = Product::find($item['id']);
                    $product->value = $product->value - $item['quantity'];
                    $product->save();

                    $inventory['product_id'] = $product->id;
                    $inventory['units_of_measure_id'] = $product->unit_of_measure_id;
                    $inventory['quantity'] =  $item['quantity'];
                    $inventory['user_id'] = Auth::user()->id;
                    $inventory['tenant_id'] = Auth::user()->tenant_id;
                    $inventory['type'] = 'Sale';
                    $inventory['direction'] = 'Deduction';
        
                    InventoryMovement::create($inventory);


                }
    
                $payment['user_id'] = Auth::user()->id;
                $payment['tenant_id'] = Auth::user()->tenant_id;
                $payment['order_id'] = $order->id;
                $payment['method'] = $request->payment['method'];
                if ($request->payment['confirmation']) {
                    $payment['reference'] = $request->payment['confirmation'];
                }
                $payment['amount'] = $request->value;
                Payment::create($payment);
    
                $response = [
                    'success' => true,
                    'data' => $order,
                ];
                return response()->json($response, 200);            
            } catch (Exception $th) {
                $response = [
                    'success' => false,
                    'message' => 'Could not create order',
                    'error'=> $th
                ];
                return response()->json($response, 500);    
            }


        }
    }

    public function status(Request $request)
    {
        $rules = [
            'order_id' => 'required',
            'title' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            try {

                if ($request->status != '') {
                    $orderStatus['title'] = $request->status;
                } else {
                    $orderStatus['title'] = 'New';
                }
    
                $orderStatus['user_id'] = Auth::user()->id;
                $orderStatus['tenant_id'] = Auth::user()->tenant_id;
                $orderStatus['order_id'] = $request->order_id;
                $orderStatus['title'] = $request->title;
                $status = OrderStatus::create($orderStatus);
    
                $response = [
                    'success' => true,
                    'data' => $status,
                ];
                return response()->json($response, 200);            
            } catch (Exception $th) {
                $response = [
                    'success' => false,
                    'message' => 'Could not update order',
                    'error'=> $th
                ];
                return response()->json($response, 500);    
            }


        }
    }

    public function payment(Request $request)
    {
        $rules = [
            'order_id' => 'required',
            'amount' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            try {

                if ($request->status != '') {
                    $orderStatus['title'] = $request->status;
                } else {
                    $orderStatus['title'] = 'New';
                }
    
                $orderStatus['user_id'] = Auth::user()->id;
                $orderStatus['tenant_id'] = Auth::user()->tenant_id;
                $orderStatus['order_id'] = $request->order_id;
                $orderStatus['title'] = $request->title;
                $status = OrderStatus::create($orderStatus);
    
                $response = [
                    'success' => true,
                    'data' => $status,
                ];
                return response()->json($response, 200);            
            } catch (Exception $th) {
                $response = [
                    'success' => false,
                    'message' => 'Could not update order',
                    'error'=> $th
                ];
                return response()->json($response, 500);    
            }


        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'employee_id' => 'required',
            'start' => 'required',
            'end' => 'required',
            'days' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $order = Order::find($request->id);
            $order->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $order = Order::find($id);
        $order->status = 1;
        $order->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }
}