<?php

namespace App\Http\Controllers;

use App\Product;
use App\Ingredient;
use App\UnitOfMeasure;
use App\StockItem;
use App\StockItemMovement;
use App\Category;
use App\InventoryMovement;
use Exception;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Settings;
use Illuminate\Support\Facades\View;
use Mail;
use App\Mail\MailModel;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($products); $i++) {
            $products[$i]->unit = UnitOfMeasure::find($products[$i]->unit_of_measure_id);
            $products{$i}->ingredients;
            $products{$i}->category;
        }
        $response = [
            'success' => true,
            'data' => $products
        ];
        return response()->json($response, 200);
    }

    public function inventory($id)
    {
        $product = Product::find($id);
        $inventory = InventoryMovement::where('status', 0)->where('product_id', $id)->get();

        for ($i = 0; $i < sizeof($inventory); $i++) {
            $inventory[$i]->unit = UnitOfMeasure::find($inventory[$i]['units_of_measure_id']);
        }

        $response = [
            'success' => true,
            'data' => [
                'product' => $product,
                'inventory' => $inventory
            ]
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $product = Product::find($id);
        for ($i = 0; $i < sizeof($product->ingredients); $i++) {
            $product->ingredients[$i]->unit = UnitOfMeasure::find($product->ingredients[$i]->unit_of_measure_id);
            $product->ingredients[$i]->stockItem = StockItem::find($product->ingredients[$i]->stock_items_id);
        }
        $product->ingredients;
        $product->unit = UnitOfMeasure::find($product->unit_of_measure_id);
        $product->category = Category::find($product->category_id);

        $response = [
            'success' => true,
            'data' => $product
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $products = Product::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($products); $i++) {
            $products[$i]->user;
        }
        return response()->json($products, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'price' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['image'] = 'Not Set';
            $product = Product::create($data);
            $response = [
                'success' => true,
                'data' => $product
            ];
            return response()->json($response, 201);
        }
    }

    public function ingredient(Request $request)
    {
        $rules = [
            'product_id' => 'required',
            'unit_of_measure_id' => 'required',
            'stock_items_id' => 'required',
            'quantity' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $ingredient = Ingredient::create($data);
            $response = [
                'success' => true,
                'data' => $ingredient
            ];
            return response()->json($response, 201);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $product = Product::find($request->id);
            $product->fill($data)->save();
            $response = [
                'success' => true,
                'data' => $product
            ];
            return response()->json($response, 200);
        }
    }

    public function prepare(Request $request)
    {
        $rules = [
            'product_id' => 'required',
            'quantity' => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            try {
                $data = $request->all();
                $data['user_id'] = Auth::user()->id;
                $data['tenant_id'] = Auth::user()->tenant_id;
                $product = Product::find($request->product_id);

                $productIngredients = [];
                $itemsForDeduction = [];
                $outOfStockItems = [];
        
                if ($product->id != '') {
                    $ingredients = Ingredient::where('status', 0)->where('product_id', $request->product_id)->get();
                    if (sizeof($ingredients) > 0) {
                        for ($i=0; $i < sizeof($ingredients); $i++) { 
                            $stockItem = StockItem::find($ingredients[$i]->stock_items_id);
                            $stockItem['quantityRequired'] = $ingredients[$i]->quantity * $request->quantity;
                            array_push($productIngredients, $stockItem);
                        }
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => 'No ingredients found',
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'product was not found',
                    ], 400);
                }

                for ($i=0; $i < sizeof($productIngredients); $i++) { 
                    $value = $productIngredients[$i]->value;
                    if ($value >= $productIngredients[$i]->quantityRequired) {
                        $total = $productIngredients[$i]->value - $productIngredients[$i]->quantityRequired;
                        $deduct['value'] = $total;
                        $deduct['required'] = $productIngredients[$i]->quantityRequired;
                        $deduct['id'] = $productIngredients[$i]->id;
                        array_push($itemsForDeduction, $deduct);
                      } else {
                        $unit = UnitOfMeasure::find($productIngredients[$i]->unit_of_measure_id);
                        $outOfStock['name'] = $productIngredients[$i]->name;
                        $outOfStock['unit'] = $unit->name;
                        $outOfStock['value'] = $value;
                        $outOfStock['required'] = $productIngredients[$i]->quantityRequired;
                        array_push($outOfStockItems, $outOfStock);
                      }
                }

                $stockItems = [];
                $deductions = [];
                if(sizeof($itemsForDeduction) == sizeof($productIngredients)){
                    for ($i=0; $i < sizeof($itemsForDeduction); $i++) { 
                        $stockItem = StockItem::find($itemsForDeduction[$i]['id']);
                        $stockItem['value'] = $itemsForDeduction[$i]['value'];
                        $stockItem->save();
                        array_push($stockItems, $stockItem);

                        $deduction['quantity'] = $itemsForDeduction[$i]['required'];
                        $deduction['stock_items_id'] = $itemsForDeduction[$i]['id'];
                        $deduction['units_of_measure_id'] = $stockItem->unit_of_measure_id;
                        $deduction['user_id'] = Auth::user()->id;
                        $deduction['tenant_id'] = Auth::user()->tenant_id;
    
                        array_push($deductions, $deduction);

                        StockItemMovement::create($deduction);
                    }   

                    $inventory['product_id'] = $product->id;
                    $inventory['units_of_measure_id'] = $product->unit_of_measure_id;
                    $inventory['quantity'] = $request->quantity;
                    $inventory['user_id'] = Auth::user()->id;
                    $inventory['tenant_id'] = Auth::user()->tenant_id;
        
                    InventoryMovement::create($inventory);

                    $product['value'] = $product['value'] + $request->quantity;
                    $product->save();
                    
                    return response()->json([
                        'success' => true,
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'data' => $outOfStockItems,
                    ], 200);
                }
            
            } catch (Exception $e) {
                
                return response()->json([
                    'success' => false,
                    'outOfStockItems' => $outOfStockItems,
                    'exception' => $e,
                ], 500);
            }
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $product = Product::find($id);
        $product->status = 1;
        $product->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function deleteIngredient(Request $request)
    {
        $id = $request->id;
        $product = Ingredient::find($id);
        $product->status = 1;
        $product->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

    public function statusReport($id)
    {
        $product = Product::find($id);
        $projects = Ingredient::where([['product_id', '=', $id], ['file_status', '<>', 'Closed']])->get();
        $settings = Settings::where('id', Auth::user()->tenant_id)->first();
        $user = Auth::user();

        $employees = Employee::all();
        $emails = $employees->reject(function ($employee) {
            return $employee->role == 'Executive' || $employee->role == 'General' || $employee->role == 'Accounts';
        })
            ->map(function ($employee, $key) {
                return $employee->email;
            });

        $recipients = $product->email;
        foreach ($emails as $value) {
            $recipients = $recipients . ' , ' . $value;
        }

        $view = View::make('status_report', [
            'projects' => $projects,
            'settings' => $settings,
            'recipients' => $recipients
        ]);


        $contents = (string)$view;

        $toSend = [
            'subject' => 'Pasha CRM Status Report',
            'message' => $contents,
            'senderName' => $user->name,
            'senderEmail' => $user->email
        ];

        Mail::to($product->email)
            ->cc($emails)
            ->send(new MailModel($toSend));

        $response = [
            'success' => true,
            'message' => 'Status Report Sent'
        ];
        return response()->json($response, 200);
    }
}