<?php

namespace App\Http\Controllers;

use App\Project;
use App\Settings;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{

    public function clean($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public function index()
    {
        $projects = Project::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($projects); $i++) {
            $projects[$i]->client;
        }
        $response = [
            'success' => true,
            'data' => $projects
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $project = Project::find($id);
        $project->client;
        $project->documents;
        $project->progress;
        $project->payments;
        for ($i = 0; $i < sizeof($project->documents); $i++) {
            $project->documents[$i]->user;
            $project->documents[$i]->project;
        }
        for ($i = 0; $i < sizeof($project->payments); $i++) {
            $project->payments[$i]->user;
            $project->payments[$i]->project;
        }
        for ($i = 0; $i < sizeof($project->progress); $i++) {
            $project->progress[$i]->user;
            $project->progress[$i]->project;
        }
        $response = [
            'success' => true,
            'data' => $project
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $projects = Project::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i = 0; $i < sizeof($projects); $i++) {
            $projects[$i]->user;
            $projects[$i]->client;
        }
        return response()->json($projects, 200);
    }

    public function available($status, $pageSize)
    {
        $projects = Project::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);

        return response()->json($projects, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'services' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['tenant_id'] = Auth::user()->tenant_id;
            $string = str_replace(' ', '', $data['services']);
            $string = str_replace('"', "", $string);
            $string = str_replace('[', "", $string);
            $string = str_replace(']', "", $string);
            $data['services'] = $string;
            $data['file_status'] = "New";
            $count = Project::count() + 1;
            $count = str_pad($count, 5, "0", STR_PAD_LEFT);
            $settings = Settings::where('id', $data['tenant_id'])->first();
            $initials = '';
            if ($settings->initials) {
                $initials = $settings->initials . '-';
            }

            $data['reference'] = $initials . $count;
            $data['reference'] = 'BGL-' . $count;
            $project = Project::create($data);
            $response = [
                'success' => true,
                'data' => $project,
            ];
            return response()->json($response, 201);
        }
    }



    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'client_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $project = Project::find($request->id);
            $project->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function fields(Request $request)
    {
        $rules = [
            'id' => 'required',
            'fields' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $project = Project::find($request->id);
            $project->fill($data)->save();
            $response = [
                'success' => true,
                'data' => $project,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $project = Project::find($id);
        $project->status = 1;
        $project->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
