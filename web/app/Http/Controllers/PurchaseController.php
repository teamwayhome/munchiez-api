<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\PurchaseItem;
use App\StockItem;
use App\UnitOfMeasure;
use App\StockItemMovement;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

class PurchaseController extends Controller
{
    public function index()
    {
        $purchases = Purchase::where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->get();
        for ($i = 0; $i < sizeof($purchases); $i++) {
            $purchases[$i]->purchaseItems;
            $purchases[$i]->user;
        }
        $response = [
            'success' => true,
            'data' => $purchases
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $purchase = Purchase::find($id);
        $purchase->purchaseItems;
        for ($i = 0; $i < sizeof($purchase->purchaseItems); $i++) {
            $purchase->purchaseItems[$i]['unit'] = UnitOfMeasure::find($purchase->purchaseItems[$i]->unit_of_measure_id);
        }
        $purchase->user;

        $response = [
            'success' => true,
            'data' => $purchase
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize = 100)
    {
        $purchases = Purchase::where('status', 0)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize);
            
        for ($i = 0; $i < sizeof($purchases); $i++) {
            $purchases[$i][steps] = sizeof($purchases[$i]->items);
            $purchases[$i]->add = json_decode($purchases[$i]->add);
            $purchases[$i]->deduct = json_decode($purchases[$i]->deduct);
        }
        
        $response = [
            'success' => true,
            'data' => $purchases
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        /* 'comments','amount','method','reference', */
        $rules = [
            'amount' => 'required',
            'method' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = [
                'message' => 'Fields Validation Failed.',
                'success' => true,
                'errors' => implode(",", $validator->messages()->all())
            ];
            return response()->json($response, 422);
        } else {
            $data = $request->all();
            $data['tenant_id'] = Auth::user()->tenant_id;
            $data['user_id'] = Auth::user()->id;
            $purchase = Purchase::create($data);
            foreach ($request->items as $item) {
                $purchaseItem['tenant_id'] = Auth::user()->tenant_id;
                $purchaseItem['user_id'] = Auth::user()->id;
                $purchaseItem['unit_of_measure_id'] = $item['unitId'];
                $purchaseItem['purchase_id'] = $purchase['id'];
                $purchaseItem['stock_items_id'] = $item['id'];
                $purchaseItem['name'] = $item['name'];
                $purchaseItem['price'] = $item['price'];
                $purchaseItem['quantity'] = $item['quantity'];

                $stockItem = StockItem::find($item['id']);
                $stockItem->value = $stockItem->value + $item['quantity'];
                
                $stockItem->save();
                PurchaseItem::create($purchaseItem);

                $addition['quantity'] = $item['quantity'];
                $addition['stock_items_id'] = $item['id'];
                $addition['units_of_measure_id'] = $item['unitId'];
                $addition['user_id'] = Auth::user()->id;
                $addition['tenant_id'] = Auth::user()->tenant_id;
                $addition['direction'] = 'Addition';
                $addition['type'] = 'Purchase';
                StockItemMovement::create($addition);
            }
            $response = [
                'success' => true,
                'data' => $purchase
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $purchase = Purchase::find($id);
        $purchase->status = 1;
        $purchase->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }
}