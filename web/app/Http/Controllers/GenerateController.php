<?php

namespace App\Http\Controllers;

use App;
use Auth;
use App\Document;
use App\DocumentItem;
use App\Payment;
use App\Progress;
use App\Settings;
use App\User;
use App\Mail\MailModel;
use Barryvdh\Snappy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mail;
use PDF;

class GenerateController extends Controller
{

    public function generateHtml($id)
    {
        $doc = Document::find($id);
        $signature = false;
        $balance = false;
        $balanceAmount = 0;
        $subject = '';

        if ($doc) {
            $document = $doc;
            $settings = Settings::where('id', $document->tenant_id)->first();
            $document->user;
            $document->client;
            $document->project;
            $document->sections;
            $document->hasComments = true;
            $subject = $document->name . ' - ' . $document->type . ' - ' . $document->project->reference;
        } else {
            $payment = Payment::find($id);
            $settings = Settings::where('tenant_id', $payment->tenant_id)->get();
            $payment->project;
            $sectionDetails = array();
            $details = array(
                'position' => 1,
                'title' => $payment->narration,
                'description' => $payment->method . ' - ' . $payment->reference,
                'quantity' => 1,
                'amount' => $payment->amount,
                'calculate' => true
            );
            array_push($sectionDetails, $details);
            $balance = true;
            $subject = $payment->narration . ' - receipt - ' . strtok($payment->id, '-');

            $document = array(
                'id' => $payment->id,
                'reference' => strtok($payment->id, '-'),
                'name' => $payment->narration,
                'description' => $payment->narration,
                'type' => 'receipt',
                'service' => 'Receipt',
                'hasComments' => false,
                'document_status ' => (object)null,
                'created_at' => $payment->created_at,
                'client' => $payment->client,
                'project' => $payment->project,
                'user' => $payment->user,
                'balance' => $balance,
                'sections' => (object)$sectionDetails,
                'balanceAmount' => $payment->balance,
            );
        }

        $document = (object)$document;

        if ($document->type === 'receipt') {
            $columns = array('Description', 'Quantity', 'Amount');
            $headers = array('Description', 'Quantity', 'Amount');
            $calculates = 'amount';
        } else {
            $documentItem = DocumentItem::where('name', $document->type)->first();
            $calculates = $documentItem->calculates;
            $columnString = str_replace(' ', '', $documentItem->columns);
            $columnString = str_replace('"', "", $columnString);
            $columnString = str_replace('[', "", $columnString);
            $columnString = str_replace(']', "", $columnString);

            $headerString = str_replace(' ', '', $documentItem->headers);
            $headerString = str_replace('"', "", $headerString);
            $headerString = str_replace('[', "", $headerString);
            $headerString = str_replace(']', "", $headerString);

            $columns = explode(',', $columnString);
            $headers = explode(',', $headerString);
        }
        switch ($document->type) {
            case 'delivery-note':
                $signature = true;
                break;
            case 'receipt':
                $balance = true;
                break;
        }

        $total = 0;
        $dSections = json_decode(json_encode($document->sections), true);
        if ($document->sections) {
            for ($i = 0; $i < sizeof($document->sections); $i++) {
                if ($dSections[$i]['calculate']) {
                    $total = $total + $dSections[$i][$calculates];
                }
            }
        }

        $document->sections = $dSections;

        $pdf = array(
            'document' => (object)$document,
            'settings' => $settings,
            'totalAmount' => $total,
            'balanceAmount' => $balanceAmount,
            'balance' => $balance,
            'columns' => $columns,
            'headers' => $headers,
            'signature' => $signature,
            'subject' => $subject,
            'contact' => null
        );

        return $pdf;
    }

    public function pdfview($id)
    {
        $tenant_id = Auth::user()->tenant_id;
        $snappy = App::make('snappy.pdf.wrapper');
        $pdf = $this->generateHtml($id);
        $logourl = $pdf['settings']['logo'];
        $prefix = "images/";
        $index = strpos($logourl, $prefix) + strlen($prefix);
        $imageFileName = substr($logourl, $index);
        $pdf['settings']['logo'] = 'file:\\\\\\' . public_path() . '\uploads' . '\\' . $tenant_id . '\images\\' . $imageFileName;
        $contact = $pdf['document']->client->contacts->first();
        $pdf['contact'] = ($contact) ? (object)$contact : null;
        $pdf['recipients'] = null;

        $view = View::make('email', $pdf);
        $contents = (string)$view;

        $myPublicFolder = public_path();
        $savePath = $myPublicFolder . '/' . 'uploads/' . $tenant_id . '/documents' . '/';
        $path = $savePath . $id . '.pdf';

        $snappy->generateFromHtml($contents, $path, [], $overwrite = true);

        return response()->json([
            'success' => 'You have successfully generated the document',
            'data' => url('/') . '/' . 'uploads/' . $tenant_id . '/' . 'documents/' . $id . '.pdf'
        ], 200);
    }

    public function htmlview($id)
    {
        $pdf = $this->generateHtml($id);
        $contact = $pdf['document']->client->contacts->first();
        $pdf['contact'] = ($contact) ? (object)$contact : null;
        $pdf['recipients'] = null;
        return View::make('email', $pdf);
    }

    public function mail($id)
    {
        $tenant_id = Auth::user()->tenant_id;
        $pdf = $this->generateHtml($id);
        $pdf['recipients'] = null;
        $view = View::make('email', $pdf);
        $contents = (string)$view;

        $settings = Settings::where('id', $tenant_id)->first();
        $client = $pdf['document']->client;
        $receivingEmail = $client['email'];
        $subject = $pdf['subject'];
        $data = [
            'subject' => $subject,
            'message' => $contents,
            'senderName' => $settings->name,
            'senderEmail' => $settings->email
        ];

        try {
            Mail::to($receivingEmail)->send(new MailModel($data));
            $data['description'] = $subject . ' email sent';
            $data['stage'] = 'Complete';
            $data['service'] = $pdf['document']->service;
            $data['project_id'] = $pdf['document']->project['id'];
            $data['client_id'] = $pdf['document']->client['id'];
            $data['user_id'] = Auth::user()->id;
            Progress::create($data);
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response, 400);
        }

    }
}
