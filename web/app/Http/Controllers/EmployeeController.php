<?php

namespace App\Http\Controllers;
Use App\Employee;
use App\Settings;
use App\User;
use Hash;
use App\Mail\MailModel;
use Mail;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::where('status',0)->where('tenant_id',Auth::user()->tenant_id)->get();
        $response = [
                'success' => true,
                'data' => $employees
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $employee = Employee::find($id);
        $employee->user;

        $response = [
                'success' => true,
                'data' => $employee
        ];
        return response()->json($response, 200);
    }

    public function pages($pageSize)
    {
        $employees = Employee::where('status',0)->where('tenant_id',Auth::user()->tenant_id)->orderBy('created_at', 'desc')->paginate($pageSize);
        for ($i=0; $i < sizeof($employees); $i++) {
            $employees[$i]->user;
        }
        return response()->json($employees, 200);
    }

    public function findBy($param,$value)
    {
      $employee = Employee::where($param,$value)->first();
      return response()->json($employee, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'role' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $password = substr(md5(microtime()),rand(0,26),7);
            $user = new User;
            $user->name = $data['name'];
            $user->password = Hash::make($password);
            $user->type = $data['role'];
            $user->email = $data['email'];
            $user->save();
            $data['user_id'] = $user->id;
            $employee = Employee::create($data);


            $settings = Settings::first();
            $view = View::make('notifications', ['name' => $data['name'], 'password' => $password, 'email' => $data['email'], 'recipients'=> $data['email'] ]);
            $contents = (string)$view;
            $toSend = [
                'subject' => 'Welcome to Pasha CRM Operations Platform',
                'message' => $contents,
                'senderName' => $settings->name,
                'senderEmail' => $settings->email
            ];

            try {
                Mail::to($data['email'])->send(new MailModel($toSend));

                $response = [
                    'success' => true,
                ];
                return response()->json($response, 200);
            }

            //catch exception
            catch(Exception $e) {

            $response = [
                'success' => true,
                'data' => $employee
            ];
            return response()->json($response, 201);
        }
      }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            $response = [
                'message'=>'Fields Validation Failed.',
                'success' => true,
                'errors' =>  implode(",",$validator->messages()->all())
            ];
            return response()->json($response, 422);
        }
        else{
            $data = $request->all();
            $employee = Employee::find($request->id);
            $employee->fill($data)->save();
            $response = [
                'success' => true,
            ];
            return response()->json($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $id =$request->id;
        $employee = Employee::find($id);
        $employee->status = 1;
        $employee->save();
        $response = [
            'success' => true,
        ];
        return response()->json($response, 200);
    }

}
