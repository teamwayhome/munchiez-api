<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class OrderStatus extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id',
        'title',
        'order_id',
        'unit_of_measure_id',
        'user_id','status','tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function order()
    {
      return $this->belongsTo('App\Order');
    }

    public function unitOfMeasure()
    {
      return $this->belongsTo('App\UnitOfMeasure');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

}