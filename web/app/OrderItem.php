<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class OrderItem extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id',
        'price',
        'quantity',
        'amount',
        'selling_price',
        'total',
        'product_id',
        'order_id',
        'unit_of_measure_id',
        'user_id','status','tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function order()
    {
      return $this->hasMany('App\Order');
    }

    public function product()
    {
      return $this->belongsTo('App\Product');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

}