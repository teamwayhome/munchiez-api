<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class UnitOfMeasure extends Model
{
    public $incrementing = false;
    protected $table = 'units_of_measure';

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id', 'name', 'user_id', 'status', 'tenant_id'
    ];
}