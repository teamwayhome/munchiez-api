<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class StockItem extends Model
{
  public $incrementing = false;

  public function deductions()
  {
    return $this->hasMany('App\StockItemDeduction');
  }

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function user(){
    return $this->belongsTo('App\User');
  }

  public function unit()
  {
    return $this->belongsTo('App\UnitOfMeasure');
  }

  public static function boot()
  {
      parent::boot();
      self::creating(function ($model) {
          $model->id = (string) Uuid::generate(4);
      });
  }

  protected $fillable = [
    'id', 
    'reference', 
    'name',
    'user_id',
    'unit_of_measure_id',
    'process_id',
    'status',
    'isTracked',
    'reorder',
    'value',
    'tenant_id',
    'category_id',
    'source_id'
  ];


}