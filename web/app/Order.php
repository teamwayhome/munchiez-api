<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Order extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id',
        'reference',
        'type',
        'value',
        'user_id','status','tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function orderItems()
    {
      return $this->hasMany('App\OrderItem');
    }

    public function statuses()
    {
      return $this->hasMany('App\OrderStatus');
    }

    public function payments()
    {
      return $this->hasMany('App\Payment');
    }
    
    public function user()
    {
      return $this->belongsTo('App\User');
    }

}