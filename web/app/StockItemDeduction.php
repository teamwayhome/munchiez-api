<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class StockItemDeduction extends Model
{
  public $incrementing = false;

  public function stockItem()
  {
    return $this->belongsTo('App\StockItem');
  }

  public static function boot()
  {
      parent::boot();
      self::creating(function ($model) {
          $model->id = (string) Uuid::generate(4);
      });
  }

  protected $fillable = [
    'id', 
    'price', 
    'quantity',
    'units_of_measure_id',
    'stock_items_id',
    'status',
    'user_id',
    'tenant_id'
  ];


}