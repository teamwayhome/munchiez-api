<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Ingredient extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id', 'quantity', 'stock_items_id', 'unit_of_measure_id', 
        'product_id', 'user_id', 'status', 'tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    protected $hidden = [];
}