<?php

namespace App;

use Webpatser\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }

    protected $fillable = [
        'name', 
        'price', 
        'type', 
        'image', 
        'isVariable', 
        'unit_of_measure_id', 
        'category_id',
        'value',
        'value', 
        'status', 'tenant_id', 'user_id'
    ];

    public function ingredients()
    {
        return $this->hasMany('App\Ingredient');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}