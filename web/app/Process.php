<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;


class Process extends Model
{
  public $incrementing = false;

  protected $fillable = [
    'id', 'name', 'add', 'deduct','sections', 'multiplier', 'user_id', 'status', 'type', 'tenant_id', 'process_id'
  ];

  public static function boot()
  {
    parent::boot();
    self::creating(function ($model) {
      $model->id = (string)Uuid::generate(4);
    });
  }

  public function sections()
  {
    return $this->hasMany('App\ProcessSection');
  }

  protected $hidden = ['status'];
}