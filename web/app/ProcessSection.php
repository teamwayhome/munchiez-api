<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class ProcessSection extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id', 'position', 'title', 'type', 
        'process_id', 'user_id', 'status', 'tenant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }

    public function process()
    {
        return $this->belongsTo('App\Process');
    }

    protected $hidden = ['status'];
}