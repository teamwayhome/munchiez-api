<?php

namespace App;

use Webpatser\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'name', 'tagline', 'initials', 'phone', 'email', 'website', 'address',
        'primary_color', 'secondary_color', 'logo', 'tax', 'document_slogan', 'status', 'beneficiary',
        'bank_name', 'bank_address', 'bank_account', 'bank_swift', 'bank_code', 'bank_branch', 'tenant_id', 'urls'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }
    protected $hidden = [];

}