<?php

namespace App;
use Webpatser\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public $incrementing = false;

    protected $fillable = [ 'id',
    'comments','amount','method','reference',
    'status','user_id', 'tenant_id'  ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function purchaseItems()
    {
      return $this->hasMany('App\PurchaseItem');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}