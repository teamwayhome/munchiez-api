<?php

namespace App\Imports;

use App\UnitOfMeasure;
use Maatwebsite\Excel\Concerns\ToModel;

class RatesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Rate([
            'description' => $row[0],
            'group' => $row[1],
            'cost' => $row[2]
        ]);
    }
}
