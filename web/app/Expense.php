<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Expense extends Model
{
    public $incrementing = false;
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'method',
        'reference',
        'amount',
        'description',
        'status','user_id', 'tenant_id'
    ];
}