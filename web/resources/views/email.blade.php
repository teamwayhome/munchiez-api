<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <title>{{ $document->name }} - {{ $document->type }} - {{$document->project->reference}}</title>
    <style>
      a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,mark,menu,nav,object,output,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,var,video{margin:0;padding:0;border:0;font:inherit;font-size:100%;vertical-align:baseline}html{line-height:1}table{border-collapse:collapse;border-spacing:0}caption,td,th{text-align:left;font-weight:normal;vertical-align:middle}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:"";content:none}a img{border:none}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}body,html{}.clearfix{display:block;clear:both}.hidden{display:none}.separator{height:15px}.separator.less{height:10px !important}#container{font:normal 15px/1.4em 'PT Sans', Sans-serif;margin:0 auto;padding:50px 65px;min-height:1058px;position:relative}#memo{min-height:100px}#memo .logo{float:right;height:75px;}#memo .logo img{height:100px;}#memo .company-info{float:left;margin-top:8px;max-width:515px}#memo .company-info span{display: block;min-width: 15px;max-width: 300px;}#memo .company-info > span:first-child{color:black;font-weight:bold;font-size:28px;line-height:1em}#memo:after{content:'';display:block;clear:both}#invoice-info{float:left;margin-top:50px}#invoice-info > div{float:left}#invoice-info > div > span{display:block;min-width:100px;min-height:18px;margin-bottom:3px}#invoice-info > div:last-child{margin-left:10px}#invoice-info:after{content:'';display:block;clear:both}#client-info{float:right;margin-top:9px;margin-right:30px;min-width:220px}#client-info > div{margin-bottom:3px}#client-info span{display:block}#client-info .client-name{font-weight:bold}#invoice-title-number{float:left;margin:60px 0 10px}#invoice-title-number span{display:inline-block;min-width:15px;line-height:1em}#invoice-title-number #title{font-size:50px;color:{{$settings->primary_color}}}#invoice-title-number #number{font-size:22px}table{table-layout:fixed}table td,table th{vertical-align:top;word-break:keep-all;word-wrap:break-word}#items{margin-top:40px}#items .first-cell,#items table td:first-child,#items table th:first-child{width:18px;text-align:right}#items table{border-collapse:separate;width:100%}#items table td span:empty,#items table th span:empty{display:inline-block}#items table th{font-weight:bold;padding:10px;text-align:right;border-bottom:2px solid #898989}#items table th:nth-child(2){width:60%;text-align:left}#items table th:last-child{text-align:right}#items table td{border-bottom:1px solid #C4C4C4;padding:10px;text-align:right}#items table td:first-child{text-align:left}#items table td:nth-child(2){text-align:left}#sums{float:right;margin-top:10px;page-break-inside:avoid}#sums table tr td,#sums table tr th{min-width:100px;padding:10px;text-align:right}#sums table tr th{text-align:left;padding-right:25px}#sums table tr.amount-total th{text-transform:uppercase;color:#331e6c}#sums table tr.amount-total td,#sums table tr.amount-total th{font-weight:bold;font-size:16px;border-top:2px solid #898989}#sums table tr:last-child th{text-transform:uppercase;color:#396E00}#sums table tr:last-child td,#sums table tr:last-child th{font-size:16px;font-weight:bold}#terms{margin-top:80px;page-break-inside:avoid}#terms > span{font-weight:bold}#terms > div{color:#331e6c;font-size:16px;min-height:70px;width:100%;max-width:500px}.payment-info{font-size:12px;margin-top:20px;width:100%;max-width:540px}.payment-info div{display:block;min-width:15px}.bottom-circles{width:310px;height:215px;-moz-background-size:384px auto;-o-background-size:384px auto;-webkit-background-size:384px auto;background-size:384px auto;position:absolute;bottom:0;right:0;overflow:hidden}.bottom-circles section{position:relative}.bottom-circles section div{-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;display:inline-block;position:absolute}.bottom-circles section > div:first-child{background:#331e6c;left:3px;width:125px;height:125px;top:117px}.bottom-circles section > div:first-child > div{background:#5c963c;width:60px;height:60px;top:32px;left:32px;position:relative}.bottom-circles section > div:last-child{background:#331e6c;right:-65px;width:280px;height:280px}.bottom-circles section > div:last-child > div{background:#5c963c;top:24px;left:24px;width:230px;height:230px}.bottom-circles section > div:last-child > div > div{background:#331e6c;top:58px;left:58px;width:115px;height:115px}.bottom-circles section > div:last-child > div > div > div{background:#FFF;top:12px;left:12px;width:90px;height:90px}.show-mobile{display:none !important}.keep-together{page-break-inside:avoid;}.break-before{page-break-before:always;padding-top:20px;columns:2;font-size:5px;}.break-after{page-break-after: always;columns:2; font-size: 5px;}.stamp{width:300px !important;z-index:-1;float:right;}.stamp img{width:100%}h2{text-align:center}.break-before ol,  .break-before p{ padding: 0; margin: 0;line-height: 18px; }
    </style>
</head>

<body>
  <div id="container">
    <section id="memo">
       <div class="logo">
         <img src="{{ $settings->logo }}" alt="{{ $settings->name }}">
       </div>

       <div class="company-info">
         <span>{{$settings->name}}</span>
         <div class="separator less"></div>

         <span>{{ $settings->address }}</span>
         <span>{{ $settings->email }}</span>
         <span>{{ $settings->phone }}</span>
         <br>
         <span>{{$settings->website}}</span>
         <p>{{$settings->tagline}}</p>
       </div>
     </section>

     <section id="invoice-title-number">

        <span id="title">{{ucfirst($document->type)}}</span>
        <div class="separator"></div>
        <span id="number">{{$document->reference}}</span>

      </section>

      <div class="clearfix"></div>

      <section id="invoice-info">
        <div>
          <span>Date:</span>
          <span> Description:</span>
        </div>

        <div>
          <span>{{$document->created_at->format('l jS \\of F Y')}}</span>
          <span>{{$document->description}}</span>
        </div>
      </section>
      <section id="client-info">
        <span>{{ $document->type == 'delivery-note' ? 'Delivered To' : 'Bill to' }}</span>
        <div>
          <span class="client-name">{{$document->client->name}}</span>
        </div>

        <!-- <div>
          <span>{client_address}</span>
        </div> -->

        <div>
          <span>{{$document->client->phone}}</span>
        </div>

        <div>
          <span>{{$document->client->email}}</span>
        </div>

        <div>
          <span>{{$document->client->website}}</span>
        </div>
        @if($contact)
        <div>
          <span>ATTN: {{ $contact->name }}</span>
        </div>
        @endif

      </section>

      <div class="clearfix"></div>

      <section id="items">

       <table cellpadding="0" cellspacing="0">
          <thead>
             <tr>
                <th>#</th>
                 @foreach ($headers as $col)
                 <th><span class="show-mobile">{{ $col }}</span> <span>{{ $col }}</span></th>
                 @endforeach
             </tr>
           </thead>
           <tbody>
             @foreach ($document->sections as $value)
             <tr data-iterate="item">
               <td>{{ $loop->index+1 }}</td> <!-- Don't remove this column as it's needed for the row commands -->
               @if(in_array('Item', $columns))
               <td><span class="show-mobile">{{ $value['title']  }}</span> 
               <span>{{ $value['title']  }}</span></td>
               @endif
               @if(in_array('Description', $columns))
               <td><span class="show-mobile">{{ $value['description'] }}</span> <span>{{ $value['description'] }}</span></td>
               @endif
               @if(in_array('Quantity', $columns))
               <td><span class="show-mobile">{{ $value['quantity'] }}</span> <span>{{ $value['quantity'] }}</span></td>
               @endif
               @if(in_array('UnitPrice', $columns))
               <td><span class="show-mobile">{{ $value['amount'] / $value['quantity'] }}</span> <span>{{ $value['amount'] / $value['quantity'] }}</span></td>
               @endif
               @if(in_array('Amount', $columns))
               <td><span class="show-mobile">{{ $value['amount'] }}</span> <span>{{ $value['amount'] }}</span></td>
               @endif
             </tr>
             @endforeach
           </tbody>
       </table>

     </section>


     <section id="sums">
       @if($document->type != 'manifest' && $document->type != 'delivery-note')
       <table cellpadding="0" cellspacing="0">
         <tr>
           <th>Sub Total</th>
           <td>{{$totalAmount}}</td>
         </tr>

         @if($document->balance)
         <tr data-hide-on-quote="true">
           <th>Balance</th>
           <td>{{$balanceAmount}}</td>
         </tr>
         @endif

         <tr data-hide-on-quote="true">
           <th>Total</th>
           <td>{{$totalAmount}}</td>
         </tr>

       </table>
       @endif
     </section>

     <div class="clearfix"></div>

    @if($document->type != 'receipt' && $document->type == 'requisition')
    <tr>
          <td>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
                  <tr>
                      <td style="padding: 10px 30px 20px 30px; font-family: sans-serif; line-height: 15px; ">
                          <strong>Requested by: </strong> {{ $document->user->name }}
                          <br>
                          <strong>Status: {{ $document['document_status']}}</strong> <br><small>{{$document['comments']}}</small>
                          <br>
                          <strong>Status change by: {{ $document->document_user }}</strong>
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
      @endif

     @if($document->hasComments  && $document->type != 'requisition')
      <section id="terms">
        <span class="hidden">Terms</span>
        <div>{{$document['comments']}}</div>
         
      </section>
      @endif



      @if($signature)
      <tr>
          <td>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
                  <tr>
                      <td style="padding: 10px 30px 20px 30px; font-family: sans-serif; font-size: 10px; line-height: 15px; ">
                          <strong>Delivered by</strong>
                          <br>
                          <hr>
                          <br>
                          <strong>Received by</strong>
                          <br>
                          <hr>
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
      @endif
      @if($document->type == 'invoice')
      <div class="payment-info">
        <div>Beneficiary Name: {{$settings->beneficiary}}</div>
        <div>Bank Name: {{$settings->bank_name}}</div>
        <div>Bank A/C #: {{$settings->bank_account}}</div>
        <div>Branch: {{$settings->bank_branch}}</div>
        <div>This document was prepared by: {{$document->user['name']}}</div>
      </div>
      @endif
  
    </div>
    @if($recipients)
    <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" class="email-container">
        <tr>
            <td style="text-align: center;padding: 20px 30px;font-family: sans-serif; font-size: 11px; line-height: 18px;color: #888888;">
                <strong>This document has been sent to: {{$recipients}} </strong> The information transmitted, including
                attachments, is intended only for the person(s) or entity to which it is addressed and may contain
                confidential and/or privileged material. Any review, retransmission, dissemination or other use of,
                 or taking of any action in reliance upon this information by persons or entities other
                 than the intended recipient is prohibited.

                 If you received this in error, please contact the sender and destroy any copies of this information.
                <br><br>
                <hr style="height:0; border:0; border-bottom:1px solid #ddd;">
            </td>
        </tr>
    </table>
    @endif
  </body>
</html>
