<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>
    <style type="text/css">
    * {
    	-webkit-font-smoothing: antialiased;
    }
    body {
    	Margin: 0;
    	padding: 0;
    	min-width: 100%;
    	font-family: Arial, sans-serif;
    	-webkit-font-smoothing: antialiased;
    	mso-line-height-rule: exactly;
    }
    table {
    	border-spacing: 0;
    	color: #333333;
    	font-family: Arial, sans-serif;
    }
    img {
    	border: 0;
    }
    .wrapper {
    	width: 100%;
    	table-layout: fixed;
    	-webkit-text-size-adjust: 100%;
    	-ms-text-size-adjust: 100%;
    }
    .webkit {
    	max-width: 600px;
    }
    .outer {
    	Margin: 0 auto;
    	width: 100%;
    	max-width: 600px;
    }
    .full-width-image img {
    	width: 100%;
    	max-width: 600px;
    	height: auto;
    }
    .inner {
    	padding: 10px;
    }
    p {
    	Margin: 0;
    	padding-bottom: 10px;
    }
    .h1 {
    	font-size: 21px;
    	font-weight: bold;
    	Margin-top: 15px;
    	Margin-bottom: 5px;
    	font-family: Arial, sans-serif;
    	-webkit-font-smoothing: antialiased;
    }
    .h2 {
    	font-size: 18px;
    	font-weight: bold;
    	Margin-top: 10px;
    	Margin-bottom: 5px;
    	font-family: Arial, sans-serif;
    	-webkit-font-smoothing: antialiased;
    }
    .one-column .contents {
    	text-align: left;
    	font-family: Arial, sans-serif;
    	-webkit-font-smoothing: antialiased;
    }
    .one-column p {
    	font-size: 14px;
    	Margin-bottom: 10px;
    	font-family: Arial, sans-serif;
    	-webkit-font-smoothing: antialiased;
    }
    .two-column {
    	text-align: center;
    	font-size: 0;
    }
    .two-column .column {
    	width: 100%;
    	max-width: 300px;
    	display: inline-block;
    	vertical-align: top;
    }
    .contents {
    	width: 100%;
    }
    .two-column .contents {
    	font-size: 14px;
    	text-align: left;
    }
    .two-column img {
    	width: 100%;
    	max-width: 280px;
    	height: auto;
    }
    .two-column .text {
    	padding-top: 10px;
    }
    .three-column {
    	text-align: center;
    	font-size: 0;
    	padding-top: 10px;
    	padding-bottom: 10px;
    }
    .three-column .column {
    	width: 100%;
    	max-width: 200px;
    	display: inline-block;
    	vertical-align: top;
    }
    .three-column .contents {
    	font-size: 14px;
    	text-align: center;
    }
    .three-column img {
    	width: 100%;
    	max-width: 180px;
    	height: auto;
    }
    .three-column .text {
    	padding-top: 10px;
    }
    .img-align-vertical img {
    	display: inline-block;
    	vertical-align: middle;
    }
    @media only screen and (max-device-width: 480px) {
    table[class=hide], img[class=hide], td[class=hide] {
    	display: none !important;
    }
    .contents1 {
    	width: 100%;
    }
    }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    	<style type="text/css">
    		table {border-collapse: collapse !important;}
    	</style>
    	<![endif]-->
  </head>

  <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0;">
    <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
      <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
        <tr>
          <td width="100%"><div class="webkit" style="max-width:600px;Margin:0 auto;">

              <!--[if (gte mso 9)|(IE)]>

    						<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0" >
    							<tr>
    								<td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
    								<![endif]-->

              <!-- ======= start main body ======= -->
              <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:600px;">
                <tr>
                  <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><!-- ======= start header ======= -->

                    <table border="0" width="100%" cellpadding="0" cellspacing="0"  >
                      <tr>
                        <td><table style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                              <tr>
                                <td align="center"><center>
                                    <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" style="Margin: 0 auto;">
                                      <tbody>
                                        <tr>
                                          <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#FFFFFF"><!-- ======= start header ======= -->

                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f2f0">
                                              <tr>
                                                <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;font-size:0;" ><!--[if (gte mso 9)|(IE)]>
                        													<table width="100%" style="border-spacing:0" >
                        													<tr>
                        													<td width="20%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:30px;" >
                        													<![endif]-->

                                                    <div class="column" style="width:100%;max-width:80px;display:inline-block;vertical-align:top;">
                                                      <table class="contents" style="border-spacing:0; width:100%"  >
                                                        <tr>
                                                          <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:5px;" align="left"><a href="#" target="_blank"><img src="{{ $logo }}" alt="" width="245" height="80" style="border-width:0; max-width:245px;height:auto; display:block" align="left"/></a></td>
                                                        </tr>
                                                      </table>
                                                    </div>

                                                  <!--[if (gte mso 9)|(IE)]>
                      													</td><td width="80%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                      													<![endif]-->



                                                                    <!--[if (gte mso 9)|(IE)]>
                      													</td>
                    													</tr>
                  													</table>
              													<![endif]--></td>
                                          </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </center></td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                    </table>

                    <!-- ======= end header ======= -->

                    <!-- ======= start hero image ======= --><!-- ======= end hero image ======= -->

                    <!-- ======= start hero article ======= -->

                    <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0; border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5; border-bottom:1px solid #e8e7e5; border-top:1px solid #e8e7e5" bgcolor="#FFFFFF">
                      <tr>
                        <td align="left" style="padding:50px 50px 50px 50px"><p style="color:#262626; font-size:24px; text-align:left; font-family: Verdana, Geneva, sans-serif"><strong>Requisition Alert</strong> <br>{{$document->user->name}} </p>
                          <p style="color:#000000; font-size:16px; text-align:left; font-family: Verdana, Geneva, sans-serif; line-height:22px ">A new requisition has been made on Pasha CRM Operations Platform. <br />
                            <br />
                            <br />
                          </p>
                          <p>Please see details below:</p>
                          <p>Client: {{$document->client->name}}</p>
                          <p>Job: {{$document->project->reference}}</p>
                          <p>Document reference: {{$document->reference}}</p>
                          <table border="0" align="left" cellpadding="3" cellspacing="3" style="Margin:0 auto;border-left:1px solid #e8e7e5;  border-top:1px solid #e8e7e5;" width="100%">
                             <thead>
                                <tr>
                                   <th>#</th>
                                    @foreach ($columns as $col)
                                    <th><span>{{ $col }}</span></th>
                                    @endforeach
                                </tr>
                              </thead>
                              <tbody>
                                @php
                                $total = 0
                                @endphp
                                @foreach ($document->sections as $value)
                                <tr data-iterate="item">
                                  <td style="border-right:1px solid #e8e7e5;border-bottom:1px solid #e8e7e5;">{{ $loop->index+1 }}</td> <!-- Don't remove this column as it's needed for the row commands -->
                                  <td style="border-right:1px solid #e8e7e5;border-bottom:1px solid #e8e7e5;"><span>{{ $value['description'] .' '. $value['title'] }}</span></td>
                                  <td align="right"  style="border-right:1px solid #e8e7e5;border-bottom:1px solid #e8e7e5;"><span>{{ $value['quantity'] }}</span></td>
                                  @if(in_array('Rate', $columns))
                                  <td align="right"  style="border-right:1px solid #e8e7e5;border-bottom:1px solid #e8e7e5;"><span>{{ $value['amount'] / $value['quantity'] }}</span></td>
                                  @endif
                                  @if(in_array('Amount', $columns))
                                  <td align="right" style="border-right:1px solid #e8e7e5;border-bottom:1px solid #e8e7e5;"><span>{{ $value['amount'] }}</span></td>
                                  @endif
                                </tr>
                                @endforeach
                                <tr data-iterate="item">
                                  <td colspan="6" align="right">{{ $total }}</td>
                                </tr>
                              </tbody>
                          </table>
                          <br />
                          <br />
                          <br />
                          <br />
                          <table border="0" align="left" cellpadding="0" cellspacing="0" style="Margin:20px auto;">
                            <tbody>
                              <tr>
                                <td align="center"><table border="0" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                    <tr>
                                      <td width="250" height="60" align="center" bgcolor="#008000" 
                                      style="-moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px;"><a href="{{ $review }}" style="width:250; display:block; text-decoration:none; border:0; text-align:center; font-weight:bold;font-size:18px; font-family: Arial, sans-serif; color: #ffffff" class="button_link">Review</a></td>
                                    </tr>
                                  </table></td>
                              </tr>
                            </tbody>
                          </table>
                          <p style="color:#000000; font-size:16px; text-align:left; font-family: Verdana, Geneva, sans-serif; line-height:22px "><br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            Best Regards, <br />
                            Pasha CRM Platform</p></td>
                      </tr>
                    </table>

                    <!-- ======= end hero article ======= -->
                </tr>
              </table>
              <!--[if (gte mso 9)|(IE)]>
    					</td>
    				</tr>
    			</table>
    			<![endif]-->
            </div></td>
        </tr>
      </table>
  </center>
  <!-- Email Container : END -->
                <!-- Footer : BEGIN -->
                @if($recipients)
    <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" class="email-container">
        <tr>
            <td style="text-align: center;padding: 20px 30px;font-family: sans-serif; font-size: 11px; line-height: 18px;color: #888888;">
                <strong>This document has been sent to: {{$recipients}} </strong> The information transmitted, including attachments, is intended only for the person(s) or entity to which it is addressed and may contain confidential and/or privileged material. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon this information by persons or entities other than the intended recipient is prohibited. If you received this in error, please contact the sender and destroy any copies of this information.
                <br><br>
                <hr style="height:0; border:0; border-bottom:1px solid #ddd;">
            </td>
        </tr>
    </table> 
    @endif
  </body>
</html>
