<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@store');
Route::post('login', 'Auth\LoginController@login');
Route::get('verify/{pin}', 'Auth\LoginController@verify');
Route::get('print/{id}', 'GenerateController@pdfview');
Route::get('html/{id}', 'GenerateController@htmlview');
Route::post('settings', 'SettingsController@store');

Route::post('login/reset', 'Auth\LoginController@reset');

Route::middleware('auth:api')
->get('/user', function (Request $request) {
    return $request->user();// Auth::guard('api')->user();
  });

Route::group(['middleware' => 'auth:api'], function() {
  Route::post('settings/logo', 'SettingsController@logo');
  
  Route::post('login/change', 'Auth\LoginController@change');
  Route::get('settings', 'SettingsController@index');

  Route::post('logout', 'Auth\LoginController@logout');
  Route::post('search', 'SearchController@index');
  Route::get('dashboard/expected/{year}', 'DashboardController@expected');
  Route::get('dashboard/payments/{year}', 'DashboardController@payments');
  Route::get('dashboard/ratios', 'DashboardController@ratios');

    //payments
  Route::get('payments', 'PaymentController@index');
  Route::get('payments/pages/{pageSize}/from/{from}/to/{to}', 'PaymentController@pages');
  Route::get('payments/{id}', 'PaymentController@show');
  Route::post('payments', 'PaymentController@store');
  Route::post('payments/balance', 'PaymentController@balance');    

  //expenses
  Route::get('expenses', 'ExpenseController@index');
  Route::get('expenses/pages/{pageSize}/from/{from}/to/{to}', 'ExpenseController@pages');
  Route::get('expenses/{id}', 'ExpenseController@show');
  Route::post('expenses', 'ExpenseController@store');
  Route::post('expenses/balance', 'ExpenseController@balance');

   //users
  Route::get('users', 'UserController@index');
  Route::get('users/pages/{pageSize}', 'UserController@pages');
  Route::get('users/{id}', 'UserController@show');
  Route::post('users', 'UserController@store');
  Route::post('users/update', 'UserController@update');
  Route::post('users/delete', 'UserController@delete');

  //units-of-measure
  Route::get('units-of-measure', 'UnitOfMeasureController@index');
  Route::get('units-of-measure/pages/{pageSize}', 'UnitOfMeasureController@pages');
  Route::get('units-of-measure/{id}', 'UnitOfMeasureController@show');
  Route::post('units-of-measure', 'UnitOfMeasureController@store');
  Route::post('units-of-measure/update', 'UnitOfMeasureController@update');
  Route::post('units-of-measure/delete', 'UnitOfMeasureController@delete');

  Route::get('processes', 'ProcessController@index');
  Route::get('processes/pages/{pageSize}', 'ProcessController@pages');
  Route::get('processes/{id}', 'ProcessController@show');
  Route::post('processes', 'ProcessController@store');
  Route::post('processes/update', 'ProcessController@update');
  Route::post('processes/delete', 'ProcessController@delete');

  Route::get('stock-items', 'StockItemController@index');
  Route::get('stock-items/pages/{pageSize}', 'StockItemController@pages');
  Route::get('stock-items/{id}', 'StockItemController@show');
  Route::get('stock-items/history/{id}', 'StockItemController@history');
  Route::post('stock-items', 'StockItemController@store');
  Route::post('stock-items/update', 'StockItemController@update');
  Route::post('stock-items/delete', 'StockItemController@delete');
  Route::post('stock-items/prepare', 'StockItemController@prepare');
  Route::get('stocktake', 'StockItemController@stocktake');
  Route::post('stocktake/record', 'StockItemController@record');
  Route::get('records', 'StockItemController@records');
  Route::get('records/{id}', 'StockItemController@details');
 
  Route::get('purchases', 'PurchaseController@index');
  Route::get('purchases/pages/{pageSize}', 'PurchaseController@pages');
  Route::get('purchases/{id}', 'PurchaseController@show');
  Route::post('purchases', 'PurchaseController@store');
  Route::post('purchases/update', 'PurchaseController@update');
  Route::post('purchases/delete', 'PurchaseController@delete');

  Route::get('products', 'ProductController@index');
  Route::get('products/pages/{pageSize}', 'ProductController@pages');
  Route::get('products/{id}', 'ProductController@show');
  Route::post('products', 'ProductController@store');
  Route::post('products/ingredient', 'ProductController@ingredient');
  Route::post('products/ingredient/delete', 'ProductController@deleteIngredient');
  Route::post('products/update', 'ProductController@update');
  Route::post('products/delete', 'ProductController@delete');
  Route::post('products/prepare', 'ProductController@prepare');
  Route::get('inventory/{id}', 'ProductController@inventory');


  Route::get('clients', 'ClientController@index');
  Route::get('clients/pages/{pageSize}', 'ClientController@pages');
  Route::get('clients/{id}', 'ClientController@show');
  Route::post('clients', 'ClientController@store');
  Route::post('clients/update', 'ClientController@update');
  Route::post('clients/delete', 'ClientController@delete');
  Route::get('clients/statusreport/{id}', 'ClientController@statusReport');

  Route::get('orders', 'OrderController@index');
  Route::get('order-items', 'OrderController@items');
  Route::get('orders/pages/{pageSize}', 'OrderController@pages');
  Route::get('orders/{id}', 'OrderController@show');
  Route::post('orders', 'OrderController@store');
  Route::post('orders/update', 'OrderController@update');
  Route::post('orders/fields', 'OrderController@fields');
  Route::post('orders/delete', 'OrderController@delete');
  Route::post('orders/status', 'OrderController@status');

   //documents
  Route::get('documents', 'DocumentController@index');
  Route::get('documents/pages/{pageSize}', 'DocumentController@pages');
  Route::get('documents/service/{service}/project/{project}', 'DocumentController@service');
  Route::get('documents/{id}', 'DocumentController@show');
  Route::get('documents/notify/{id}', 'DocumentController@notify');
  Route::post('documents/status', 'DocumentController@status');
  Route::post('documents', 'DocumentController@store');
  Route::post('documents/update', 'DocumentController@update');
  Route::post('documents/delete', 'DocumentController@delete');

   //sections
  Route::get('sections', 'SectionController@index');
  Route::get('sections/pages/{pageSize}', 'SectionController@pages');
  Route::get('sections/{id}', 'SectionController@show');
  Route::post('sections', 'SectionController@store');
  Route::post('sections/update', 'SectionController@update');
  Route::post('sections/delete', 'SectionController@delete');

   //services
  Route::get('services', 'ServiceController@index');
  Route::get('services/pages/{pageSize}', 'ServiceController@pages');
  Route::get('services/{id}', 'ServiceController@show');
  Route::post('services', 'ServiceController@store');
  Route::post('services/update', 'ServiceController@update');
  Route::post('services/delete', 'ServiceController@delete');

    //employees
  Route::get('employees', 'EmployeeController@index');
  Route::get('employees/pages/{pageSize}', 'EmployeeController@pages');
  Route::get('employees/{id}', 'EmployeeController@show');
  Route::get('employees/{param}/{value}', 'EmployeeController@findBy');
  Route::post('employees', 'EmployeeController@store');
  Route::post('employees/update', 'EmployeeController@update');
  Route::post('employees/delete', 'EmployeeController@delete');

    //leave applications
  Route::get('leave-applications', 'LeaveController@index');
  Route::get('leave-applications/pages/{pageSize}', 'LeaveController@pages');
  Route::get('leave-applications/{id}', 'LeaveController@show');
  Route::post('leave-applications', 'LeaveController@store');
  Route::post('leave-applications/update', 'LeaveController@update');
  Route::post('leave-applications/delete', 'LeaveController@delete');
  Route::post('leave-applications/approve', 'LeaveController@approve');
  Route::post('leave-applications/reject', 'LeaveController@reject');

    //uploads
  Route::get('uploads', 'UploadController@index');
  Route::get('uploads/project/{id}', 'UploadController@project');
  Route::get('uploads/{id}', 'UploadController@show');
  Route::post('uploads', 'UploadController@store');
  Route::post('uploads/delete', 'UploadController@delete');

    //rates
  Route::get('rates', 'RateController@index');
  Route::get('rates/pages/{pageSize}', 'RateController@pages');
  Route::get('rates/{id}', 'RateController@show');
  Route::post('rates', 'RateController@store');
  Route::post('rates/delete', 'RateController@delete');
  Route::post('rates/uploads', 'RateController@upload');

  Route::post('settings/update', 'SettingsController@update');

  Route::get('progress/category/{service}/project/{project}', 'ProgressController@service');
  Route::post('progress', 'ProgressController@store');
  Route::get('progress/pages/{pageSize}', 'ProgressController@pages');


    //contacts
  Route::get('contacts', 'ContactController@index');
  Route::get('contacts/pages/{pageSize}', 'ContactController@pages');
  Route::get('contacts/{id}', 'ContactController@show');
  Route::post('contacts', 'ContactController@store');
  Route::post('contacts/delete', 'ContactController@delete');
  Route::post('contacts/update', 'ContactController@update');

    //document-items
  Route::get('document-items', 'DocumentItemController@index');
  Route::get('document-items/pages/{pageSize}', 'DocumentItemController@pages');
  Route::get('document-items/{id}', 'DocumentItemController@show');
  Route::post('document-items', 'DocumentItemController@store');
  Route::post('document-items/delete', 'DocumentItemController@delete');
  Route::post('document-items/update', 'DocumentItemController@update');

    //categories
  Route::get('categories', 'CategoryController@index');
  Route::get('categories/pages/{pageSize}', 'CategoryController@pages');
  Route::get('categories/{id}', 'CategoryController@show');
  Route::post('categories', 'CategoryController@store');
  Route::post('categories/delete', 'CategoryController@delete');
  Route::post('categories/update', 'CategoryController@update');

  Route::get('mail/{id}', 'GenerateController@mail');

});