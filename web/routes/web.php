<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('html/{id}', 'GenerateController@htmlview');
Route::get('pdf/{id}', 'GenerateController@pdfview');
Route::get('requisition/review/{id}', 'DocumentController@review');
Route::get('leave/review/{id}', 'LeaveController@review');
Route::post('requisition', 'DocumentController@requisition')->name('requisition');
Route::post('leave-request', 'LeaveController@leaveRequest')->name('leave-request');
